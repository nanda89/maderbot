package com.ebco.bot.maderbot;

public class ExplicitException extends CeriaException {

    public ExplicitException(String message) {
        super(message);
    }

}
