package com.ebco.bot.maderbot;

import android.os.Environment;
import android.telephony.CellIdentityGsm;
import android.telephony.CellIdentityLte;
import android.telephony.CellIdentityWcdma;
import android.telephony.CellInfo;
import android.telephony.CellInfoCdma;
import android.telephony.CellInfoGsm;
import android.telephony.CellInfoLte;
import android.telephony.CellInfoWcdma;
import android.telephony.CellSignalStrengthCdma;
import android.telephony.CellSignalStrengthGsm;
import android.telephony.CellSignalStrengthLte;
import android.telephony.CellSignalStrengthWcdma;
import android.telephony.TelephonyManager;
import android.telephony.gsm.GsmCellLocation;
import android.util.Log;

import androidx.test.uiautomator.UiDevice;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

import java.io.File;
import java.lang.reflect.Type;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Util {

    private static final String TAG = "TEST-BOT";
    public static final DateFormat DF = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    public static final DecimalFormat SF = new DecimalFormat("##.##");
    public static final String IMAGE_FOLDER_NAME = "JeniusAPM";

    private static Gson gson = null;

    public static Gson getGson() {
        if (gson == null) {
            JsonSerializer<Date> ser = new JsonSerializer<Date>() {
                @Override
                public JsonElement serialize(Date src, Type typeOfSrc,
                                             JsonSerializationContext context) {
                    return src == null ? null
                            : new JsonPrimitive(src.getTime());
                }
            };

            JsonDeserializer<Date> deser = new JsonDeserializer<Date>() {
                @Override
                public Date deserialize(JsonElement json, Type typeOfT,
                                        JsonDeserializationContext context)
                        throws JsonParseException {
                    return json == null ? null : new Date(json.getAsLong());
                }
            };

            gson = new GsonBuilder().registerTypeAdapter(Date.class, ser)
                    .registerTypeAdapter(Date.class, deser)
                    .disableHtmlEscaping().create();
        }
        return gson;
    }

    static String getNetworkType(TelephonyManager tm) {
        int networkType = tm.getNetworkType();
        switch (networkType) {
            case TelephonyManager.NETWORK_TYPE_GPRS:
            case TelephonyManager.NETWORK_TYPE_EDGE:
            case TelephonyManager.NETWORK_TYPE_CDMA:
            case TelephonyManager.NETWORK_TYPE_1xRTT:
            case TelephonyManager.NETWORK_TYPE_IDEN:
                return "2G";
            case TelephonyManager.NETWORK_TYPE_UMTS:
            case TelephonyManager.NETWORK_TYPE_EVDO_0:
            case TelephonyManager.NETWORK_TYPE_EVDO_A:
            case TelephonyManager.NETWORK_TYPE_HSDPA:
            case TelephonyManager.NETWORK_TYPE_HSUPA:
            case TelephonyManager.NETWORK_TYPE_HSPA:
            case TelephonyManager.NETWORK_TYPE_EVDO_B:
            case TelephonyManager.NETWORK_TYPE_EHRPD:
            case TelephonyManager.NETWORK_TYPE_HSPAP:
                return "3G";
            case TelephonyManager.NETWORK_TYPE_LTE:
                return "4G";
            default:
                return "NotFound";
        }
    }

    static double getSignalLevel(TelephonyManager tm) {
        double result = 0;
        List<CellInfo> cellInfoList = tm.getAllCellInfo();

        if (cellInfoList != null) {
            for (final CellInfo cellInfo : cellInfoList) {
                if (cellInfo instanceof CellInfoGsm) {
                    CellSignalStrengthGsm cellSignalStrengthGsm = ((CellInfoGsm) cellInfo).getCellSignalStrength();
                    result = cellSignalStrengthGsm.getDbm();

                } else if (cellInfo instanceof CellInfoCdma) {
                    CellSignalStrengthCdma cellSignalStrengthCdma = ((CellInfoCdma) cellInfo).getCellSignalStrength();
                    result = cellSignalStrengthCdma.getCdmaDbm();
                } else if (cellInfo instanceof CellInfoLte) {
                    CellSignalStrengthLte cellSignalStrengthLte = ((CellInfoLte) cellInfo).getCellSignalStrength();
                    result = cellSignalStrengthLte.getDbm();

                } else if (cellInfo instanceof CellInfoWcdma) {
                    CellSignalStrengthWcdma cellSignalStrengthWcdma = ((CellInfoWcdma) cellInfo).getCellSignalStrength();
                    result = cellSignalStrengthWcdma.getDbm();
                }
            }
        }
        Log.v(TAG, "getSignalLevel: result of signal level is " + result);
        return result;
    }

    static int getBandwith(TelephonyManager tm) {
        int result = 0;
        List<CellInfo> cellInfoList = tm.getAllCellInfo();

        if (cellInfoList != null) {
            for (final CellInfo cellInfo : cellInfoList) {
                if (cellInfo instanceof CellInfoLte) {
                    CellIdentityLte cellIdentityLte = ((CellInfoLte) cellInfo).getCellIdentity();
                    result = cellIdentityLte.getBandwidth();
                }
            }
        }
        return result;
    }
    static String getOperator(TelephonyManager tm) {
        String result = "unknown";
        List<CellInfo> cellInfoList = tm.getAllCellInfo();
        Log.d(TAG, "getOperator: 1111111111");
        if (cellInfoList != null) {
            Log.d(TAG, "getOperator: 22222222222");
            for (final CellInfo cellInfo : cellInfoList) {
                Log.d(TAG, "getOperator: 3333333333");
                if (cellInfo instanceof CellInfoLte) {
                    Log.d(TAG, "getOperator: 44444444aaaaaaaa");
                    CellIdentityLte cellIdentityLte = ((CellInfoLte) cellInfo).getCellIdentity();
                    result = cellIdentityLte.getMobileNetworkOperator();
                }else if (cellInfo instanceof CellInfoWcdma) {
                    Log.d(TAG, "getOperator: 4444444444bbbbbbb");
                    CellIdentityWcdma cellIdentityWcdma = ((CellInfoWcdma) cellInfo).getCellIdentity();
                    result = cellIdentityWcdma.getMobileNetworkOperator();
                }else if(cellInfo instanceof CellInfoGsm){
                    Log.d(TAG, "getOperator: 444444444ccccccc");
                    CellIdentityGsm cellIdentityGsm = ((CellInfoGsm) cellInfo).getCellIdentity();
                    result = cellIdentityGsm.getMobileNetworkOperator();
                }
            }
        }
        return result;
    }

    static int getSignalIndex(TelephonyManager tm){
        int result = 0;
        List<CellInfo> cellInfoList = tm.getAllCellInfo();

        if (cellInfoList != null) {
            for (final CellInfo cellInfo : cellInfoList) {
                if (cellInfo instanceof CellInfoGsm) {
                    CellSignalStrengthGsm cellSignalStrengthGsm = ((CellInfoGsm) cellInfo).getCellSignalStrength();
                    result = cellSignalStrengthGsm.getLevel();
                } else if (cellInfo instanceof CellInfoCdma) {
                    CellSignalStrengthCdma cellSignalStrengthCdma = ((CellInfoCdma) cellInfo).getCellSignalStrength();
                    result = cellSignalStrengthCdma.getLevel();
                } else if (cellInfo instanceof CellInfoLte) {
                    CellSignalStrengthLte cellSignalStrengthLte = ((CellInfoLte) cellInfo).getCellSignalStrength();
                    result = cellSignalStrengthLte.getLevel();

                } else if (cellInfo instanceof CellInfoWcdma) {
                    CellSignalStrengthWcdma cellSignalStrengthWcdma = ((CellInfoWcdma) cellInfo).getCellSignalStrength();
                    result = cellSignalStrengthWcdma.getLevel();
                }
            }
        }

        return result;
    }
    static int getSignalQuality(TelephonyManager tm){
        int result = 0;
        List<CellInfo> cellInfoList = tm.getAllCellInfo();
        if (cellInfoList != null) {
            for (final CellInfo cellInfo : cellInfoList) {
                 if (cellInfo instanceof CellInfoLte) {
                    CellSignalStrengthLte cellSignalStrengthLte = ((CellInfoLte) cellInfo).getCellSignalStrength();
                    result = cellSignalStrengthLte.getRsrq();
                }
            }
        }
        return result;
    }
    static int getCqi(TelephonyManager tm){
        int result = 0;
        List<CellInfo> cellInfoList = tm.getAllCellInfo();

        if (cellInfoList != null) {
            for (final CellInfo cellInfo : cellInfoList) {
                if (cellInfo instanceof CellInfoLte) {
                    CellSignalStrengthLte cellSignalStrengthLte = ((CellInfoLte) cellInfo).getCellSignalStrength();
                    result = cellSignalStrengthLte.getCqi();
                }else {
                    continue;
                }
            }
        }
        return result;
    }
    static int getArfnc(TelephonyManager tm){
        int result = 0;
        List<CellInfo> cellInfoList = tm.getAllCellInfo();
        if (cellInfoList != null) {
            for (final CellInfo cellInfo : cellInfoList) {
                if (cellInfo instanceof CellInfoLte) {
                    CellIdentityLte cellIdentityLte = ((CellInfoLte) cellInfo).getCellIdentity();
                    result = cellIdentityLte.getEarfcn();
                } else if (cellInfo instanceof CellInfoWcdma) {
                    CellIdentityWcdma cellIdentityWcdma = ((CellInfoWcdma) cellInfo).getCellIdentity();
                    result = cellIdentityWcdma.getUarfcn();
                }else if(cellInfo instanceof  CellInfoGsm){
                    CellIdentityGsm cellIdentityGsm = ((CellInfoGsm) cellInfo).getCellIdentity();
                    result = cellIdentityGsm.getArfcn();
                }
            }
        }

        return  result;
    }
    static int getRssnr(TelephonyManager tm){
        int result = 0;
        List<CellInfo> cellInfoList = tm.getAllCellInfo();
        if (cellInfoList != null) {
            for (final CellInfo cellInfo : cellInfoList) {
                if (cellInfo instanceof CellInfoLte) {
                    CellSignalStrengthLte cellSignalStrengthLte = ((CellInfoLte) cellInfo).getCellSignalStrength();
                    result = cellSignalStrengthLte.getRssnr();
                }
            }
        }

        return result;
    }
    static int getTimingAdvanced(TelephonyManager tm){
        int result = 0;
        List<CellInfo> cellInfoList = tm.getAllCellInfo();
        if (cellInfoList != null) {
            for (final CellInfo cellInfo : cellInfoList) {
                if (cellInfo instanceof CellInfoLte) {
                    CellSignalStrengthLte cellSignalStrengthLte = ((CellInfoLte) cellInfo).getCellSignalStrength();
                    result = cellSignalStrengthLte.getTimingAdvance();
                }
            }
        }
        return result;
    }
    static int getCellId(TelephonyManager tm) {
        if (tm.getPhoneType() == TelephonyManager.PHONE_TYPE_GSM) {
            final GsmCellLocation location = (GsmCellLocation) tm.getCellLocation();
            if (location != null) {
                return location.getCid();
            }
        }
        return 0;
    }
    static int getCID(TelephonyManager tm){
        int result = 0;
        List<CellInfo> cellInfoList = tm.getAllCellInfo();

        if (cellInfoList != null) {
            for (final CellInfo cellInfo : cellInfoList) {
                if (cellInfo instanceof CellInfoLte) {
                    CellIdentityLte cellIdentityLte = ((CellInfoLte) cellInfo).getCellIdentity();
                    result = cellIdentityLte.getCi();
                } else if (cellInfo instanceof CellInfoWcdma) {
                    CellIdentityWcdma cellIdentityWcdma = ((CellInfoWcdma) cellInfo).getCellIdentity();
                    result = cellIdentityWcdma.getCid();
                }
            }
        }

        return  result;
    }

    static Long getWaitingTime(String ping) {
        String[] waitingTime = ping.split(",");
        String w = waitingTime[waitingTime.length - 1];

        Pattern p = Pattern.compile("\\d+");
        Matcher m = p.matcher(w);
        String result = null;
        while (m.find()) {
            Log.d(TAG, "ping ->> " + m.group());
            result = m.group();
        }

        long finalResult;
        if (result == null) {
            finalResult = 0L;
        } else {
            finalResult = Long.valueOf(result);

        }
        return finalResult;
    }

    public static String takePicture(UiDevice device) {
        SimpleDateFormat f = new SimpleDateFormat("yyyyMMddhhmmss");
        String date = f.format(new Date());
        String fileName = date + "_Error.jpg";

        File folder = new File(Environment.getExternalStorageDirectory() +
                File.separator + IMAGE_FOLDER_NAME);
        if (!folder.exists()) {
            boolean status = folder.mkdirs();
            // Log.d(TAG, "create folder successful " + status);
        }

        File file = new File(Environment.getExternalStorageDirectory() + "/" +
                IMAGE_FOLDER_NAME + "/" + fileName);
        device.takeScreenshot(file, 0.5f, 25);

        // Log.d(TAG, "File " + file.getName() + " exist? -> " + file.exists());
        return fileName;
    }

}
