package com.ebco.bot.maderbot;

public class CeriaException extends Exception {

    public CeriaException(String message) {
        super(message);
    }

}
