package com.ebco.bot.maderbot;

public class LoadingException extends CeriaException {

    public LoadingException(String message) {
        super(message);
    }

}
