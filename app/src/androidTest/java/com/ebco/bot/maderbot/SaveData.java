package com.ebco.bot.maderbot;

import android.content.Context;
import android.os.Environment;
import android.util.Log;

import com.ebco.bot.maderbot.db.DatabaseApp;
import com.ebco.bot.maderbot.dto.JourneyDTO;
import com.ebco.bot.maderbot.dto.JourneyDetailDTO;
import com.ebco.bot.maderbot.dto.JourneyErrorDTO;
import com.ebco.bot.maderbot.dto.JourneyNVTDTO;
import com.ebco.bot.maderbot.model.JourneyDetailMaster;
import com.ebco.bot.maderbot.pojo.ResponseJourney;
import com.ebco.bot.maderbot.pojo.ResponseJourneyDetail;
import com.google.gson.Gson;

import java.io.File;
import java.io.IOException;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import okhttp3.FormBody;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;

import static androidx.test.core.app.ApplicationProvider.getApplicationContext;

public class SaveData {

    private static final String TAG = "SENDDATA-BOT";

//    public static final String HOST_JENIUS = "http://192.168.77.155:8080/jenius";
    private static final String HOST_JENIUS = "http://poc.ebconnect.com:8080/jenius";
    private static final String REST_API_JOURNEY = HOST_JENIUS + "/api/v1/Journey";
    private static final String REST_API_JOURNEY_DETAIL = HOST_JENIUS + "/api/v1/journeyDetail";
    private static final String REST_API_JOURNEY_NVT = HOST_JENIUS + "/api/v1/journeyNVT";
    private static final String REST_API_JOURNEY_ERROR = HOST_JENIUS + "/api/v1/journeyError";

    private static List<JourneyDTO> journeys = new ArrayList<>();
    private static List<JourneyDetailDTO> journeyDetails = new ArrayList<>();
    private static List<JourneyErrorDTO> journeyErrors = new ArrayList<>();
    private static List<JourneyNVTDTO> journeyNVTs = new ArrayList<>();
    private static List<String> errorMessages = new ArrayList<>();

    private static void clean() {
        journeys.clear();
        journeyDetails.clear();
        journeyErrors.clear();
        journeyNVTs.clear();
        errorMessages.clear();
    }

    public static void saveJourneyResults(String token ,Long journeyMasterId, String journeyName,
                                          String operator, double latitude, double longitude,
                                          String networkType, int cellId, double signalLevel,
                                          long responseTime, String nvtTime, String nvtDuration,
                                          long startTime, int nvtRepeat,
                                          List<JourneyDetailResult> results,int percentage,
                                          int uarfn,int bandwith,int signalStrength,int signalQuality,
                                          int snnr,int timingAdvance,int cqi,int signalIndex) {
        boolean hasError = false;
        for (JourneyDetailResult result : results) {
            if (result.error) {
                hasError = true;
                break;
            }
        }

        long journeyId = saveJourney(token ,journeyMasterId, latitude, longitude, cellId, startTime);

        for (JourneyDetailResult result : results) {
            saveJourneyDetailAndError(result, journeyId ,journeyMasterId, token);
        }

        saveNVT(token ,networkType, cellId, signalLevel, nvtTime, nvtDuration,
                0, "nvt", nvtRepeat, journeyId,percentage,
                operator,uarfn,bandwith,signalStrength,signalQuality,snnr,timingAdvance,cqi,signalIndex);
    }

    private static long saveJourney(String token, Long journeyMasterId,double latitude, double longitude, int cellId,long startTime) {

        String startJourney = convertTime(startTime);
        long journeyId = DatabaseService.insertIntoJourney(token, String.valueOf(journeyMasterId), String.valueOf(latitude),
                String.valueOf(longitude), startJourney,cellId,"mobile", getApplicationContext());

        return journeyId;
    }

    private static void saveJourneyDetailAndError(JourneyDetailResult journeyDetailResult, long journeyId,long journeyMasterId, String token) {
        JourneyDetailDTO page = journeyDetailResult.journeyDetail;
        long journeyDetailId = saveJourneyDetail(page, journeyId,journeyMasterId,token);

        if (journeyDetailResult.error && journeyDetailResult.journeyError != null) {
            JourneyErrorDTO je = journeyDetailResult.journeyError;
            saveJourneyError(je, journeyDetailId,journeyDetailResult);
        }
    }

    private static long saveJourneyDetail(JourneyDetailDTO detail, Long journeyId,Long journeyMasterId, String token) {

        boolean isError = false;
        if(detail.isError()){
            isError = true;
        }

        String startPageTime = convertTime(detail.getStartTime());
        Log.d(TAG, "saveJourneyDetail: get detail page by name : "+detail.getDetailName()+" and id journey master : "+journeyMasterId);
        String pageId = getIdPage( detail.getDetailName(), journeyMasterId , getApplicationContext());
        Double resTime = detail.getResponseTime() / 1000d;
        return DatabaseService.insertIntoJourneyDetail(pageId, token, detail.getNetworkType(),
                detail.getCellId(), detail.getSignalLevel(), new DecimalFormat("##.##").format(resTime), detail.getStatus(),
                detail.getMessage(),startPageTime , detail.getRepeatNo(), journeyId,
                null, isError, getApplicationContext());
    }

    private static void saveJourneyError(JourneyErrorDTO error, long journeyDetailId,
                                         JourneyDetailResult journeyDetailResult) {
        DatabaseService.insertIntoJourneyError(error.getDescription(), journeyDetailId,
                journeyDetailResult.journeyError.getMonitorJourneyDetailId(), error.getImageFilename(),
                error.getErrorClassification(), getApplicationContext());
    }

    static void saveNVT(String token,String networkType, int cellId, double signalLevel, String nvtTime,
                        String nvtDuration, int status, String message, int nvtRepeat, long journeyId , int percentage,
                        String operator,int arfn,int bandwith,int signal,int quality,
                        int snnr,int timing,int cqi,int signalIndex) {



        DatabaseService.insertIntoNVT(token, networkType, cellId, signalLevel, nvtDuration,
                status, message, nvtTime, nvtRepeat, journeyId,
                null,percentage,
                operator,arfn,bandwith,signal,quality,snnr,timing,cqi,signalIndex,
                getApplicationContext());

    }

    public static void sendPendingData(String token) {
        for (JourneyDTO journey : journeys) {
            try {
                sendJourney(journey, token);
                for (JourneyDetailDTO detail : journeyDetails) {
                    if (detail.getJourneyId().longValue() == journey.getId().longValue()) {
                        detail.setMonitorJourneyId(journey.getMonitorJourneyId());
                    }
                }
                for (JourneyNVTDTO nvt : journeyNVTs) {
                    if (nvt.getJourneyId().longValue() == journey.getId().longValue()) {
                        nvt.setMonitorJourneyId(journey.getMonitorJourneyId());
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        for (JourneyDetailDTO detail : journeyDetails) {
            try {
                sendJourneyDetail(detail, token);
                for (JourneyErrorDTO error : journeyErrors) {
                    if (error.getDetailId().longValue() == detail.getId().longValue()) {
                        error.setMonitorJourneyDetailId(detail.getMonitorJourneyId());
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        for (JourneyNVTDTO nvt : journeyNVTs) {
            try {
                sendJourneyNVT(nvt, token);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        for (JourneyErrorDTO error : journeyErrors) {
            try {
                sendError(error, token);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        clean();
    }

    static void sendJourney(JourneyDTO journey, String token) throws Exception {
        OkHttpClient client = new OkHttpClient();

        RequestBody formBody = new FormBody.Builder()
                .add("journey_master_id", "" + journey.getMasterId())
                .add("lat", "" + journey.getLatitude())
                .add("lng", "" + journey.getLongitude())
                .add("datetime", Util.DF.format(journey.getStartTime()))
                .add("cell_id", String.valueOf(journey.getCellId()))
                .build();

        Request request = new Request.Builder()
                .addHeader("token", token)
                .addHeader("Content-Type", "application/x-www-form-urlencoded")
                .url(REST_API_JOURNEY)
                .post(formBody)
                .build();

        okhttp3.Response response = client.newCall(request).execute();
        String respJson = response.body().string();
        Log.v(TAG, "Response: " + respJson);

        Gson gson = new Gson();
        ResponseJourney res = gson.fromJson(respJson, ResponseJourney.class);
        if (res.getResponseCode() == 200) {
            Long journeyId = res.getData().getJourneyId();
            journey.setMonitorJourneyId(journeyId);
        }
    }

    static void sendJourneyDetail(JourneyDetailDTO journeyDetail, String token) throws Exception {
        OkHttpClient client = new OkHttpClient();
        RequestBody formBody = new FormBody.Builder()
                .add("journey_id", "" + journeyDetail.getMonitorJourneyId())
                .add("journey_detail_master_id", "" + journeyDetail.getJourneyDetailMasterId())
                .add("network_type", journeyDetail.getNetworkType())
                .add("cell_id", "" + journeyDetail.getCellId())
                .add("signal_level", "" + (int) journeyDetail.getSignalLevel())
                .add("response_time", "" + (journeyDetail.getResponseTime() / 1000d))
                .add("command", "")
                .add("status", "" + journeyDetail.getStatus())
                .add("message", journeyDetail.getMessage())
                .add("datetime", Util.DF.format(journeyDetail.getStartTime()))
                .add("repeat_no", "" + journeyDetail.getRepeatNo())
                .build();

        Request request = new Request.Builder()
                .addHeader("token", token)
                .addHeader("Content-Type", "application/x-www-form-urlencoded")
                .url(REST_API_JOURNEY_DETAIL)
                .post(formBody)
                .build();

        okhttp3.Response response = client.newCall(request).execute();
        String respJson = response.body().string();
        Log.v(TAG, "Response: " + respJson);

        Gson gson = new Gson();
        ResponseJourneyDetail res = gson.fromJson(respJson, ResponseJourneyDetail.class);
        if (res.getResponseCode() == 200) {
            Long detailId = res.getData().getJourneyDetailId();
            journeyDetail.setMonitorJourneyId(detailId);
        }
    }

    static void sendJourneyNVT(JourneyNVTDTO nvt, String token) throws IOException {
        OkHttpClient client = new OkHttpClient();
        RequestBody formBody = new FormBody.Builder()
                .add("journey_id", "" + nvt.getMonitorJourneyId())
                .add("network_type", nvt.getNetworkType())
                .add("cell_id", "" + nvt.getCellId())
                .add("signal_level", "" + (int) nvt.getSignalLevel())
                .add("response_time", "" + (nvt.getResponseTime() / 1000d))
                .add("command", "")
                .add("status", "" + nvt.getStatus())
                .add("message", nvt.getMessage())
                .add("datetime", Util.DF.format(nvt.getStartTime()))
                .add("repeat_no", "" + nvt.getRepeatNo())
                .add("percentage", "" + nvt.getPercentage())
                .build();

        Request request = new Request.Builder()
                .addHeader("token", token)
                .addHeader("Content-Type", "application/x-www-form-urlencoded")
                .url(REST_API_JOURNEY_NVT)
                .post(formBody)
                .build();

        okhttp3.Response response = client.newCall(request).execute();
        Log.v(TAG, "Response: " + response.body().string());
    }

    static void sendError(JourneyErrorDTO error, String token) throws IOException {
        File fileError = null;
        if (error.getImageFilename() != null) {
            fileError = new File(Environment.getExternalStorageDirectory() + "/" +
                    Util.IMAGE_FOLDER_NAME + "/" + error.getImageFilename());
        }

        OkHttpClient client = new OkHttpClient();

        MultipartBody.Builder builder = new MultipartBody.Builder().setType(MultipartBody.FORM)
                .addFormDataPart("journey_detail_id", "" + error.getMonitorJourneyDetailId())
                .addFormDataPart("description", error.getDescription());
        if (fileError != null) {
            builder.addFormDataPart("image", fileError.getName(),
                    RequestBody.create(MediaType.parse("image/*"), fileError));
        }
        RequestBody formBody = builder.build();

        Request request = new Request.Builder()
                .addHeader("token", token)
                .addHeader("Content-Type", "application/x-www-form-urlencoded")
                .url(REST_API_JOURNEY_ERROR)
                .post(formBody)
                .build();

        okhttp3.Response response = client.newCall(request).execute();
        Log.v(TAG, "Response: " + response.body().string());
    }

    private static String convertTime(long time){
        Date date = new Date(time);
        SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String dateTime = f.format(date);
        return dateTime;
    }
    private static String getIdPage(String key, long journeyMasterId, Context context){
        JourneyDetailMaster detail = DatabaseApp.getDatabaseApp(context).journeyDetailMasterDao().getByNameMstId(key , journeyMasterId);
        if(detail != null){
            return detail.getId().toString();
        }else{
            return "0";
        }

    }
}
