package com.ebco.bot.maderbot;

import com.ebco.bot.maderbot.dto.JourneyDetailDTO;
import com.ebco.bot.maderbot.dto.JourneyErrorDTO;

class JourneyDetailResult {
    boolean botFailure;
    boolean error;
    JourneyDetailDTO journeyDetail;
    JourneyErrorDTO journeyError;
}
