package com.ebco.bot.maderbot;

public class EnvStatus {
    int cellId;
    double signalLevel;
    String networkType;
    String nvtStartTime;
    int nvtPercentage;
    String nvtResponseTime;
    double latitude;
    double longitude;

    String operator;
    int uarfn;
    int bandwith;
    int cellBandwith;
    int signalStrength;
    int signalQuality;
    int ssnr;
    int timingAdvance;
    int cqi;
    int SignalIndex;
}
