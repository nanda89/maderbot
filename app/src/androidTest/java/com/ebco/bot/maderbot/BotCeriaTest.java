package com.ebco.bot.maderbot;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Location;
import android.location.LocationManager;
import android.os.RemoteException;
import android.telephony.TelephonyManager;
import android.util.ArrayMap;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.test.ext.junit.runners.AndroidJUnit4;
import androidx.test.filters.SdkSuppress;
import androidx.test.uiautomator.By;
import androidx.test.uiautomator.UiDevice;
import androidx.test.uiautomator.UiObject;
import androidx.test.uiautomator.UiObject2;
import androidx.test.uiautomator.UiScrollable;
import androidx.test.uiautomator.UiSelector;
import androidx.test.uiautomator.Until;

import com.ebco.bot.maderbot.db.DatabaseApp;
import com.ebco.bot.maderbot.dto.JourneyDetailDTO;
import com.ebco.bot.maderbot.dto.JourneyErrorDTO;
import com.ebco.bot.maderbot.model.JourneyDetailMaster;
import com.ebco.bot.maderbot.model.JourneyMaster;
import com.ebco.bot.maderbot.service.SendDataService;
import com.ebco.bot.maderbot.util.CheckServiceUtil;

import org.junit.After;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static android.content.Context.TELEPHONY_SERVICE;
import static androidx.test.core.app.ApplicationProvider.getApplicationContext;
import static androidx.test.platform.app.InstrumentationRegistry.getInstrumentation;
import static com.ebco.bot.maderbot.Util.getArfnc;
import static com.ebco.bot.maderbot.Util.getBandwith;
import static com.ebco.bot.maderbot.Util.getCellId;
import static com.ebco.bot.maderbot.Util.getCqi;
import static com.ebco.bot.maderbot.Util.getNetworkType;
import static com.ebco.bot.maderbot.Util.getOperator;
import static com.ebco.bot.maderbot.Util.getRssnr;
import static com.ebco.bot.maderbot.Util.getSignalIndex;
import static com.ebco.bot.maderbot.Util.getSignalLevel;
import static com.ebco.bot.maderbot.Util.getSignalQuality;
import static com.ebco.bot.maderbot.Util.getTimingAdvanced;
import static java.lang.Thread.sleep;

@RunWith(AndroidJUnit4.class)
@SdkSuppress(minSdkVersion = 18)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class BotCeriaTest {

    private static final String TAG = "TEST-BOT";

    private static final String TARGET_APP_PACKAGE = "id.co.bri.ceria";
    private static final String MyPREFERENCES      = "MyPref";

    private static final int   LAUNCH_TIMEOUT       = 30000;
    private static       int   LOAD_TIMEOUT         = 30000;
    private static final int   FIND_TIMEOUT         = 5000;
    private static final int   TIMEOUT_8s           = 8000;
    private static final int   INPUT_DELAY          = 1000;
    private static final int   WIDGET_TIMEOUT       = 8000;
    private static final float SCROLL_PERCENTAGE    = 1.15f;
    private static final int   SCROLL_SPEED         = 2000;
    private static final int   SCROLL_DELAY         = 1000;
    private static final int   NR_OF_CYCLES_IN_HOUR = 4;

    private enum Pages {
        START_PAGE, LOGIN_PAGE,FORM_PAGE,VERIFICATION_PAGE,RESULT_PAGE,
        FORM_PENGAJUAN_PAGE,REC_VERIVICATION_PAGE,CREDIT_RESULT
    }

    private long   startTime;
    private long[] startTimes = new long[Pages.values().length];
    private long[] endTimes   = new long[Pages.values().length];
    private long[] durations  = new long[Pages.values().length];
    private int    pageIdx    = 0;
    private long   widgetStartTime;

    private static int[] repeatCounts = new int[Pages.values().length];

    static {
        Arrays.fill(repeatCounts, 1);
    }

    private Context context;
    SharedPreferences prefs;
    private TelephonyManager tm;
    private LocationManager lm;
    private UiDevice device;
    private String           urlServer;
    private String           token;
    private int              journeyDelay = 3000;
    private int              pageDelay    = 2000;
    private String           loginPassword;
    private EnvStatus        lastStatus   = new EnvStatus();
    private String           currentScreenshot;
    private static Double longitude;
    private static Double latitude;

    @Before
    public void prepare() throws InterruptedException {
        Log.d(TAG, "prepare: starting preparation before running test");
        context = getApplicationContext();
        prefs = context.getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        getDataFromPreferences();
        tm = (TelephonyManager) context.getSystemService(TELEPHONY_SERVICE);
        lm = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);

        Log.d(TAG, "prepare: getting master data from server");
        try{
            new GetDataMaster.getJourneyMaster(urlServer, token, context).execute();
            sleep(FIND_TIMEOUT);

            List<JourneyMaster> journeyMaster =
                    DatabaseApp.getDatabaseApp(context).journeyMasterDao().getAll();
            Log.d(TAG, "startMainActivityFromHomeScreen: total journey is : " + journeyMaster.size());
            for (JourneyMaster jm : journeyMaster) {
                new GetDataMaster.getJourneyDetailMaster(jm.getId(), urlServer, token, context).execute();
            }

            Log.d(TAG, "get data configuration");
            new GetConfiguration.getData(token, urlServer, context).execute();
        }catch (Exception e){
            e.printStackTrace();
        }

        // Initialize UiDevice instance
        device = UiDevice.getInstance(getInstrumentation());

        // Start from the home screen
        device.pressHome();
        sleep(FIND_TIMEOUT);

        try {
            Log.d(TAG, "prepare: clear app");
            SimpleDateFormat hourFormat = new SimpleDateFormat("HH");
            String hour = hourFormat.format(new Date());
            String[] hours = new String[]{"06", "12", "18", "00"};
            List<String> listHour = Arrays.asList(hours);
            if (listHour.contains(hour)) {
                clearApps();
            }
        } catch (RemoteException e) {
            Log.d(TAG, "prepare: " + e.getMessage());
            e.printStackTrace();
        }
    }

    @Test
    public void runEvent(){
        startTime = System.currentTimeMillis();
        Log.d(TAG, "runJourneys: journey login will start");
        boolean loginSuccess = false;
        try {
            loginSuccess = runJourneyLogin();
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (!loginSuccess) {
            return;
        }
        try {
            journeyDelay();
            joureyCredit();
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @After
    public void close() throws InterruptedException {
        long endTime = System.currentTimeMillis();
        Log.i(TAG, "Cycle finished");

        long totalCycle = (endTime - startTime) / 1000;
        Log.d(TAG, "total cycle : " + (double) totalCycle);
        new SendData.submitTotalTimeCycle(urlServer, (double) totalCycle, context, token).execute();

        Context context = getApplicationContext();
        final Intent intent =
                context.getPackageManager().getLaunchIntentForPackage("com.ebco.bot.maderbot");
        assert intent != null;
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        context.startActivity(intent);

        waitJourney();
    }
    private static void waitJourney() throws InterruptedException {
        SimpleDateFormat timeFormat = new SimpleDateFormat("mm");
        String m = timeFormat.format(new Date());
        int minute = Integer.valueOf(m);
        int mnt = 0;

        int cycleSlotTimeInMinutes = (int) Math.floor(60 / NR_OF_CYCLES_IN_HOUR);

        for (int i = 1; i <= NR_OF_CYCLES_IN_HOUR; i++) {
            if (minute < cycleSlotTimeInMinutes * i) {
                mnt = (cycleSlotTimeInMinutes * i) - minute;
                break;
            }
        }
        mnt = mnt * 1000 * 60;
        sleep(mnt);
    }


    private void joureyCredit() throws InterruptedException {
        final List<JourneyDetailResult> pageResults = new ArrayList<>();
        final EnvStatus status = getStatusNvtLocationNetwork();

        Log.d(TAG, "joureyCredit: get journey master credit");
        final JourneyMaster master = DatabaseApp.getDatabaseApp(context).journeyMasterDao()
                .getContainName("edit", "Ceria Mobile");

        final long journeyStartTime = System.currentTimeMillis();
        JourneyDetailResult formCredit = runFormCredit(master);
        pageResults.add(formCredit);

        if(!formCredit.error){
            JourneyDetailResult verification = runVerification(master);
            pageResults.add(verification);

            if(!verification.error){
                JourneyDetailResult result = runResult(master);
                pageResults.add(result);
            }
        }
        long responseTime = 0;
        for (JourneyDetailResult res : pageResults) {
            responseTime += res.journeyDetail.getResponseTime();
        }
        final long journeyResponseTime = responseTime;
        Thread thread = new Thread() {
            @Override
            public void run() {

                process(master.getId(), master.getName(), journeyResponseTime,
                        journeyStartTime, pageResults, status);

            }
        };
        thread.start();
    }

    private JourneyDetailResult runResult(JourneyMaster master) throws InterruptedException {
        JourneyDetailResult result = new JourneyDetailResult();
        EnvStatus status = updateNetworkStatus();
        pageIdx = Pages.CREDIT_RESULT.ordinal();

        JourneyDetailMaster detailMaster = DatabaseApp.getDatabaseApp(context).
                journeyDetailMasterDao().getContainName("Result", master.getId());

        try {
            String ktp = prefs.getString("ktp",null);
            String account = prefs.getString("account",null);
            String debitCard = prefs.getString("debit_card",null);
            String expireCard = prefs.getString("expire_card",null);

            device.findObject(By.res(TARGET_APP_PACKAGE,"et_ktp_apply_salary_verification")).setText(ktp);
            sleep(500);
            device.findObject(By.res(TARGET_APP_PACKAGE,"et_account_apply_salary_verification")).setText(account);
            sleep(500);
            device.findObject(By.res(TARGET_APP_PACKAGE,"et_debit_apply_salary_verification")).setText(debitCard);
            sleep(500);

            UiObject s = device.findObject(new UiSelector().resourceId("et_exp_apply_salary_verification"));
            UiScrollable scroll = new UiScrollable(new UiSelector().scrollable(true));
            scroll.scrollIntoView(s);
            sleep(500);
            device.findObject(By.res(TARGET_APP_PACKAGE,"et_exp_apply_salary_verification")).setText(expireCard);

            startTimes[pageIdx]=System.currentTimeMillis();
            device.findObject(By.res(TARGET_APP_PACKAGE , "btn_apply_salary_verification")).click();
        }catch (Exception e){
            handleCeriaException(e,result,"open result credit submission page",false , null);
        }finally {
            result.journeyDetail = createJourneyDetail(result , detailMaster.getId(),pageIdx,master.getName(), detailMaster.getName(), status);
            pageDelay();
        }
        return result;
    }

    private JourneyDetailResult runVerification(JourneyMaster master) throws InterruptedException {
        JourneyDetailResult result = new JourneyDetailResult();
        EnvStatus status = updateNetworkStatus();

        pageIdx = Pages.REC_VERIVICATION_PAGE.ordinal();

        //change : get data from database
        JourneyDetailMaster detailMaster = DatabaseApp.getDatabaseApp(context).
                journeyDetailMasterDao().getContainName("Verification", master.getId());

        try {
           device.findObject(By.res(TARGET_APP_PACKAGE,"cb_apply_req1")).click();
           sleep(500);
            device.findObject(By.res(TARGET_APP_PACKAGE,"cb_apply_req2")).click();
            sleep(500);

            UiObject2 btnContinue = device.wait(Until.findObject(By.res(TARGET_APP_PACKAGE , "btn_continue")),TIMEOUT_8s);
            if(btnContinue.isEnabled()){
                startTimes[pageIdx]=System.currentTimeMillis();
                btnContinue.click();

                device.wait(Until.findObject(By.text("Verifikasi Data Nasabah")),LOAD_TIMEOUT);
            }


        }catch (Exception e){
            handleCeriaException(e,result,"open verification page",false , null);
        }finally {
            result.journeyDetail = createJourneyDetail(result , detailMaster.getId(),pageIdx,master.getName(), detailMaster.getName(), status);
            pageDelay();
        }
        return result;
    }

    private JourneyDetailResult runFormCredit(JourneyMaster master) throws InterruptedException {
        JourneyDetailResult result = new JourneyDetailResult();
        EnvStatus status = updateNetworkStatus();
        pageIdx = Pages.FORM_PENGAJUAN_PAGE.ordinal();

        JourneyDetailMaster detailMaster = DatabaseApp.getDatabaseApp(context).
                journeyDetailMasterDao().getContainName("orm Credit", master.getId());

        try {
            UiObject2 btnCreditSubmission = device.wait(Until.findObject(
                    By.res(TARGET_APP_PACKAGE,"btn_ajukan_credit")), LOAD_TIMEOUT);
            if(btnCreditSubmission == null){
                throw new RuntimeException("Button credit submission not found");
            }
            if(!btnCreditSubmission.isEnabled()){
                throw  new RuntimeException("Button credit submission disabled");
            }
            startTimes[pageIdx]=System.currentTimeMillis();
            btnCreditSubmission.click();

            device.wait(Until.findObject(By.res(TARGET_APP_PACKAGE,"cb_apply_req1")),LOAD_TIMEOUT);


        }catch (Exception e){
            handleCeriaException(e,result,"open credit submission page",false , null);
        }finally {
            result.journeyDetail = createJourneyDetail(result , detailMaster.getId(),pageIdx,master.getName(), detailMaster.getName(), status);
            pageDelay();
        }
        return result;
    }

    private void pageDelay() throws InterruptedException {
        sleep(pageDelay);
    }

    private boolean runJourneyLogin() throws InterruptedException {
        boolean loginResult = false;
        final List<JourneyDetailResult> pageResults = new ArrayList<>();
        final EnvStatus status = getStatusNvtLocationNetwork();

        Log.d(TAG, "runJourneyLogin: get journey master login");
        //change : get from database
        final JourneyMaster master = DatabaseApp.getDatabaseApp(context).journeyMasterDao()
                .getContainName("ogi", "Ceria Mobile");

        final long journeyStartTime = System.currentTimeMillis();
        JourneyDetailResult startResult = runPageStart(master);
        pageResults.add(startResult);

        if (!startResult.error) {
            JourneyDetailResult pageDashboardResult = runPageDashboard(master);
            pageResults.add(pageDashboardResult);
            loginResult = !pageDashboardResult.error;
        }

        long responseTime = 0;
        for (JourneyDetailResult res : pageResults) {
            responseTime += res.journeyDetail.getResponseTime();
        }
        final long journeyResponseTime = responseTime;
        Thread thread = new Thread() {
            @Override
            public void run() {

                process(master.getId(), master.getName(), journeyResponseTime,
                        journeyStartTime, pageResults, status);

            }
        };
        thread.start();

        return loginResult;

    }

    /* Login to Dashboard */
    private JourneyDetailResult runPageDashboard(JourneyMaster master) throws InterruptedException {
        JourneyDetailResult result = new JourneyDetailResult();
        EnvStatus status = updateNetworkStatus();
        pageIdx = Pages.LOGIN_PAGE.ordinal();
        startTimes[pageIdx] = System.currentTimeMillis();

        //change : get data from database
        JourneyDetailMaster detailMaster = DatabaseApp.getDatabaseApp(context)
                .journeyDetailMasterDao().getContainName("Process", master.getId());

        try {
            inputPINs();

            startTimes[pageIdx] = System.currentTimeMillis();
            UiObject2 btnCredit = device.wait(Until.findObject(
                    By.res(TARGET_APP_PACKAGE,"btn_ajukan_credit")), LOAD_TIMEOUT);

            if (btnCredit == null) {
                throw new LoadingException("Dashboard content can't be accessed");
            }

        } catch (Exception e) {
            handleCeriaException(e, result, "open Dashboard page", false, null);

        } finally {
            result.journeyDetail = createJourneyDetail(result, detailMaster.getId(), pageIdx,
                    master.getName(), detailMaster.getName(), status);
        }

        return result;
    }

    // Launch App until start page is displayed
    private JourneyDetailResult runPageStart(JourneyMaster master) throws InterruptedException {
        JourneyDetailResult result = new JourneyDetailResult();
        EnvStatus status = updateNetworkStatus();
        pageIdx = Pages.START_PAGE.ordinal();
        startTimes[pageIdx] = startTime; // System.currentTimeMillis()

        //change : get data from database
        JourneyDetailMaster detailMaster = DatabaseApp.getDatabaseApp(context).
                journeyDetailMasterDao().getContainName("gin Page", master.getId());

        try {
            final Intent intent =
                    context.getPackageManager().getLaunchIntentForPackage(TARGET_APP_PACKAGE);
            assert intent != null;
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);    // Clear out any previous instances
            context.startActivity(intent);

            Log.i(TAG, "Starting ceria App...");

            // Wait for the app to appear
            startTimes[pageIdx] = System.currentTimeMillis();
            boolean appOpened = device.wait(Until.hasObject(
                    By.pkg(TARGET_APP_PACKAGE).depth(0)), LAUNCH_TIMEOUT * 2);
            if (!appOpened) {
                throw new CeriaException("Failed to open Ceria App");
            }

            UiObject2 numPad = device.wait(Until.findObject(
                    By.text("Masukkan PIN")), LOAD_TIMEOUT);
            if (numPad == null) {
                throw new LoadingException("NumPad not found");
            }else {
                Log.d(TAG, "runPageStart: numpad founded");
            }

        } catch (Exception e) {
            handleCeriaException(e, result, "open App Landing page", false, null);

        } finally {
            result.journeyDetail = createJourneyDetail(result, detailMaster.getId(), pageIdx,
                    master.getName(), detailMaster.getName(), status);
        }

        return result;
    }

    private void getDataFromPreferences() {
        urlServer = prefs.getString("url", "");
        token = prefs.getString("token", "");
        loginPassword = prefs.getString("loginPassword", "");
        journeyDelay = prefs.getInt("journey_interval", 0);
        pageDelay = prefs.getInt("page_interval", 0);

        String waitTimeOut = prefs.getString("waitTimeOut", "");
        LOAD_TIMEOUT = waitTimeOut != "" ? Integer.valueOf(waitTimeOut) : 30000;
        journeyDelay = prefs.getInt("journey_interval", 3000);

        Log.d(TAG, "getDataFromPreferences: load timeout value :"+LOAD_TIMEOUT);

    }

    private void clearApps() throws RemoteException, InterruptedException {
        device.pressRecentApps();
        sleep(1000);
        UiObject2 clearButton = device.findObject(By.text("Close all"));
        if (clearButton != null) {
            Log.d(TAG, "clearApps: button found");
            clearButton.click();
        } else {
            Log.d(TAG, "clearApps: button not found");
            device.pressHome();
        }
    }

    private EnvStatus getStatusNvtLocationNetwork() {

        EnvStatus status = new EnvStatus();

        Map<String, Object> nvt = runNVT();
        status.nvtStartTime = (String) nvt.get("nvtTime");
        status.nvtPercentage = (int) nvt.get("percentage");
        status.nvtResponseTime = (String) nvt.get("resNVT");

        Location location = lm.getLastKnownLocation(LocationManager.GPS_PROVIDER);
        latitude = (location != null) ? location.getLatitude() : 0;
        longitude = (location != null) ? location.getLongitude() : 0;

        status.latitude = latitude;
        status.longitude = longitude;
        status.cellId = getCellId(tm);
        status.signalLevel = getSignalLevel(tm);
        status.networkType = getNetworkType(tm);

        status.operator = getOperator(tm);
        status.bandwith = getBandwith(tm);
        status.cqi = getCqi(tm);
        status.SignalIndex = getSignalIndex(tm);
        status.uarfn = getArfnc(tm);
        status.signalStrength = (int) getSignalLevel(tm);
        status.signalQuality = getSignalQuality(tm);
        status.ssnr = getRssnr(tm);
        status.timingAdvance = getTimingAdvanced(tm);

        lastStatus = status;
        return status;
    }

    private EnvStatus updateNetworkStatus() {
        EnvStatus status = lastStatus;
        status.cellId = getCellId(tm);
        status.signalLevel = getSignalLevel(tm);
        status.networkType = getNetworkType(tm);
        lastStatus = status;
        currentScreenshot = null;
        return status;
    }

    private void process(long masterId, String journeyName, long journeyResponseTime,
                         long journeyStartTime, List<JourneyDetailResult> pageResults,
                         EnvStatus status) {
        SaveData.saveJourneyResults(token, masterId, journeyName, status.operator,
                status.latitude, status.longitude, status.networkType, status.cellId,
                status.signalLevel, journeyResponseTime, status.nvtStartTime,
                status.nvtResponseTime, journeyStartTime, 1, pageResults,
                status.nvtPercentage,
                status.uarfn, status.bandwith, status.signalStrength, status.signalQuality, status.ssnr,
                status.timingAdvance, status.cqi, status.SignalIndex);

        if (!CheckServiceUtil.isMyServiceRunning(SendDataService.class, getApplicationContext())) {
            getApplicationContext().startService(new Intent(getApplicationContext(),
                    SendDataService.class));
        }
    }

    private Map<String, Object> runNVT() {
        Map<String, Object> dataNVT = new ArrayMap<>();
        try {
            Runtime runtime = Runtime.getRuntime();
            Process process = runtime.exec("ping -s 1024 -c 4 -w 4 8.8.8.8");
            BufferedReader stdInput =
                    new BufferedReader(new InputStreamReader(process.getInputStream()));

            String s;
            StringBuilder res = new StringBuilder();
            StringBuilder avgRes = new StringBuilder();
            while ((s = stdInput.readLine()) != null) {
                if (s.contains("packets transmitted")) {
                    Log.d(TAG, "runNVT: ping : " + s);
                    res.append(s).append("\n");
                }
                if (s.contains("rtt")) {
                    Log.d(TAG, "runNVT: get avg res : " + s);
                    avgRes.append(s).append("\n");
                }
            }
            stdInput.close();
            process.destroy();
            int percentage = 100 - getPercentage(res.toString());
            Log.d(TAG, "runNVT: get res data from : " + avgRes.toString());
            String resNVTTime = "0";
            if (!avgRes.toString().isEmpty() || !avgRes.toString().isEmpty()) {
                resNVTTime = waitingTime(avgRes.toString());
            }

            Log.d(TAG, "runNVT: resNVTTime : " + resNVTTime);
            dataNVT.put("percentage", percentage);
            dataNVT.put("resNVT", resNVTTime);
            dataNVT.put("nvtTime", Util.DF.format(new Date()));

        } catch (IOException e) {
            e.printStackTrace();
        }

        return dataNVT;
    }

    private static int getPercentage(String ping) {
        String[] waitingTime = ping.split(",");
        String persentase = waitingTime[2];
        Pattern p = Pattern.compile("\\d+");
        Matcher m = p.matcher(persentase);

        String result = null;
        while (m.find()) {
            System.out.println("ping " + m.group());
            result = m.group();
        }

        return result != null ? Integer.parseInt(result) : 0;
    }

    private static String waitingTime(String ping) {
        String[] waitingTime = ping.split("=");
        String[] getAvgResNVT = waitingTime[1].split("/");
        String w = getAvgResNVT[1];

        Log.d(TAG, "waitingTime: response NVT : " + w);

        return w == null || w.isEmpty() ? "0" : w;
    }

    private void handleCeriaException(Exception e, JourneyDetailResult result, String action,
                                       boolean handleExplicit, String tabName) throws InterruptedException {
        String errLogMsg = "Bot Error on " + action +
                // " page " + Pages.values()[pageIdx].name() +
                " (" + e.getClass().getSimpleName() + "): " + e.getMessage();
        Log.e(TAG, errLogMsg);
//        currentScreenshot = Util.takePicture(device);

        if (e instanceof CeriaException) {
            result.error = true;
            result.journeyError = createJourneyError("Failed to " + action + " ("
                    + e.getMessage() + ")");
            if (handleExplicit) {
                handleExplicitException(e, result.journeyError, tabName);
            }
        } else {
            result.botFailure = true;
            throw new IllegalStateException(errLogMsg);
        }
    }

    private void clickBackUntilNavMenuFound() throws InterruptedException {
        UiObject2 numPad = device.findObject(By.desc("NumPad_Container"));
        if (numPad != null) {
            inputPINs();
            UiObject2 dashboard = device.wait(Until.findObject(
                    By.res(TARGET_APP_PACKAGE,"btn_ajukan_credit")), LOAD_TIMEOUT);
            if (dashboard == null) {
                throw new IllegalStateException("Failed to re-login");
            }
        }
    }

    private void inputPINs() throws InterruptedException {

        String pin = prefs.getString("pin", "");
        Log.d(TAG, "inputPINs: process input pin : "+pin);
        String key1 = pin.substring(0, 1);
        String key2 = pin.substring(1, 2);
        String key3 = pin.substring(2, 3);
        String key4 = pin.substring(3, 4);
        String key5 = pin.substring(4, 5);
        String key6 = pin.substring(5, 6);

        Log.d(TAG, "inputPINs: key1 : "+key1+" , key2 : "+key2);
        sleep(500);
        device.findObject(By.res(TARGET_APP_PACKAGE,"tv_" + key1)).click();
        sleep(500);
        device.findObject(By.res(TARGET_APP_PACKAGE,"tv_" + key2)).click();
        sleep(500);
        device.findObject(By.res(TARGET_APP_PACKAGE,"tv_" + key3)).click();
        sleep(500);
        device.findObject(By.res(TARGET_APP_PACKAGE,"tv_" + key4)).click();
        sleep(500);
        device.findObject(By.res(TARGET_APP_PACKAGE,"tv_" + key5)).click();
        sleep(500);
        device.findObject(By.res(TARGET_APP_PACKAGE,"tv_" + key6)).click();
    }

    private void handleExplicitException(Exception e, JourneyErrorDTO error, String tabName) throws InterruptedException {
        if (e instanceof ExplicitException) {
            if (e.getMessage().equals("Timeout")) {
                if (tabName != null) {
                    UiObject2 tab = device.wait(Until.findObject(By.text(tabName)), FIND_TIMEOUT);
                    if (tab != null) {
                        tab.click();
                        clickDelay();
                    }
                } else {
                    clickBackUntilNavMenuFound();
                }

            } else if (e.getMessage().contains("tidak tersedia")) { // PLN out of service time
                UiObject2 ok = device.findObject(By.textStartsWith("OK"));
                if (ok != null) {
                    ok.click();
                    clickDelay();
                }

            } else if (e.getMessage().startsWith("Popup")) {
                clickBackUntilNavMenuFound();

            } else if (e.getMessage().startsWith("Log Out")) {
                try {
                    inputPINs();
                    sleep(LOAD_TIMEOUT);
                } catch (Exception err) {
                    err.printStackTrace();
                }

            } else {
                clickBackUntilNavMenuFound();
            }

            error.setErrorClassification("Explicit");
        }
    }

    private void clickDelay() throws InterruptedException {
        sleep(100);
    }

    private JourneyErrorDTO createJourneyError(String desc) {
        return createJourneyError(desc, false);
    }

    private void journeyDelay() throws InterruptedException {
        sleep(journeyDelay);
    }

    @NonNull
    private JourneyErrorDTO createJourneyError(String desc, boolean explicit) {
//        if (currentScreenshot == null) {
//            currentScreenshot = Util.takePicture(device);
//        }

        JourneyErrorDTO error = new JourneyErrorDTO();
        error.setDescription(desc);
        error.setMonitorJourneyDetailId(0L);
//        error.setImageFilename(currentScreenshot);
        error.setErrorClassification(explicit ? "Explicit" : "Implicit");
        return error;
    }

    @NonNull
    private JourneyDetailDTO createJourneyDetail(JourneyDetailResult journeyDetailResult,
                                                 Long journeyDetailMasterId, int idx,
                                                 String journeyName, String pageName,
                                                 EnvStatus status) {
        endTimes[idx] = System.currentTimeMillis();
        durations[idx] = endTimes[idx] - startTimes[idx];

        if (journeyDetailResult.error) {
            Log.w(TAG, pageName + " error after " + durations[idx] + " ms");
        } else {
            Log.i(TAG, pageName + " displayed after " + durations[idx] + " ms");
        }

        JourneyDetailDTO page = new JourneyDetailDTO();
        page.setJourneyDetailMasterId(journeyDetailMasterId);
        page.setStartTime(startTimes[idx]);
        // page.setLocation(location);
        // page.setOperator(operator);
        page.setJourneyName(journeyName);
        page.setDetailName(pageName);
        page.setCellId(status.cellId);
        page.setLatitude(status.latitude);
        page.setLongitude(status.longitude);
        page.setNetworkType(status.networkType);
        page.setSignalLevel(status.signalLevel);
        page.setResponseTime(durations[idx]);
        page.setNvtSignal(status.signalLevel);
        page.setNvtResponseTime(Math.round(Double.valueOf(status.nvtResponseTime) * 1000));
        if (journeyDetailResult.error) {
            page.setStatus(1);
            page.setMessage("error");
            page.setError(true);
            page.setRepeatNo(repeatCounts[idx]);
            repeatCounts[idx]++;
        } else {
            page.setStatus(0);
            page.setMessage("success");
            page.setError(false);
            if (repeatCounts[idx] > 1) {
                repeatCounts[idx] = 1;
            }
            page.setRepeatNo(repeatCounts[idx]);
        }
        page.setToken(token);
        return page;
    }
}
