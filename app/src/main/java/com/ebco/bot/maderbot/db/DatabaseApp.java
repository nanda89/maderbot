package com.ebco.bot.maderbot.db;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

import com.ebco.bot.maderbot.dao.JourneyDao;
import com.ebco.bot.maderbot.dao.JourneyDetailDao;
import com.ebco.bot.maderbot.dao.JourneyDetailMasterDao;
import com.ebco.bot.maderbot.dao.JourneyErrorDao;
import com.ebco.bot.maderbot.dao.JourneyMasterDao;
import com.ebco.bot.maderbot.dao.JourneyNVTDao;
import com.ebco.bot.maderbot.model.Journey;
import com.ebco.bot.maderbot.model.JourneyDetail;
import com.ebco.bot.maderbot.model.JourneyDetailMaster;
import com.ebco.bot.maderbot.model.JourneyError;
import com.ebco.bot.maderbot.model.JourneyMaster;
import com.ebco.bot.maderbot.model.JourneyNVT;


@Database(entities = {Journey.class, JourneyDetail.class , JourneyError.class , JourneyNVT.class , JourneyMaster.class , JourneyDetailMaster.class}, version = 1, exportSchema = false)
public abstract class DatabaseApp extends RoomDatabase {
    public abstract JourneyDao journeyDao();

    public abstract JourneyDetailDao journeyDetailDao();

    public abstract JourneyErrorDao journeyErrorDao();

    public abstract JourneyNVTDao journeyNVTDao();

    public abstract JourneyMasterDao journeyMasterDao();

    public abstract JourneyDetailMasterDao journeyDetailMasterDao();

    private static DatabaseApp INSTANCE;

    public static DatabaseApp getDatabaseApp(Context context) {
        if (INSTANCE == null) {
            INSTANCE = Room.databaseBuilder(context.getApplicationContext(), DatabaseApp.class,
                    "database_ceria").allowMainThreadQueries().build();
        }

        return INSTANCE;
    }

    public static void destroyInstance() {
        INSTANCE = null;
    }
}
