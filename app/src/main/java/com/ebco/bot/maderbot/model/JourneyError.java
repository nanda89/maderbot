package com.ebco.bot.maderbot.model;

import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.Index;

@Entity(tableName = "journey_error", foreignKeys = @ForeignKey(entity = JourneyDetail.class, parentColumns =
        "id", childColumns = "detailid", onDelete = ForeignKey.CASCADE, onUpdate = ForeignKey
        .CASCADE), indices = @Index({"id", "detailid"}))
public class JourneyError extends ModelEntity {

    private String table;
    private Long monitor_journey_detail_id;
    private String description;
    private boolean isSent;
    private Long detailid;
    private String errorClassification = "Implicit";

    public String getTable() {
        return table;
    }

    public void setTable(String table) {
        this.table = table;
    }

    public Long getMonitor_journey_detail_id() {
        return monitor_journey_detail_id;
    }

    public void setMonitor_journey_detail_id(Long monitor_journey_detail_id) {
        this.monitor_journey_detail_id = monitor_journey_detail_id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean isSent() {
        return isSent;
    }

    public void setSent(boolean sent) {
        isSent = sent;
    }

    public Long getDetailid() {
        return detailid;
    }

    public void setDetailid(Long detailid) {
        this.detailid = detailid;
    }

    public String getErrorClassification() {
        return errorClassification;
    }

    public void setErrorClassification(String errorClassification) {
        this.errorClassification = errorClassification;
    }

}
