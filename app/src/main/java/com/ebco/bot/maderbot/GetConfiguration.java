package com.ebco.bot.maderbot;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.util.Log;

import com.ebco.bot.maderbot.pojo.ResponseConfig;
import com.google.gson.Gson;

import java.io.IOException;

import okhttp3.OkHttpClient;
import okhttp3.Request;

public class GetConfiguration {

    private static final String TAG = "TEST-BOT";
    private static final String TYPE_NAME = "Sinaya Mobile";
    private static final String HOUR = "hour";
    private static final String MINUTE = "minute";
    private static final String MyPREFERENCES = "MyPref";
    static final String PAGE_INTERVAL = "page_interval";
    static final String JOURNEY_INTERVAL = "journey_interval";


    @SuppressLint("StaticFieldLeak")
    public static class getData extends AsyncTask<Void, String, Boolean> {

        String token;
        Context context;
        String urlServer;

        getData(String token , String urlServer , Context context){
            super();
            this.token = token;
            this.urlServer = urlServer;
            this.context = context;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Boolean doInBackground(Void... voids) {

            OkHttpClient client = new OkHttpClient();

            Request request = new Request.Builder()
                    .url(urlServer + "/api/v1/journeyConfig")
                    .header("token", token)
                    .build();

            try {
                okhttp3.Response response = client.newCall(request)
                        .execute();

                String responseMessage = response.body().string();
                Log.d(TAG, "onResponse: response from configuration : " + responseMessage);
                Gson gson = new Gson();
                ResponseConfig resConfig = gson.fromJson(responseMessage, ResponseConfig.class);

                Log.d(TAG, "doInBackground: responseCode : "+resConfig.getResponseCode()+" , status : "+resConfig.isStatus());


                if (resConfig.getResponseCode() == 200) {
                    for(int i = 0 ; i < resConfig.getData().getConfigs().size();i++){
                        SharedPreferences sharedPreferences = context.getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
                        SharedPreferences.Editor editor = sharedPreferences.edit();

                        String value = resConfig.getData().getConfigs().get(i).getValue();
                        String timeType = resConfig.getData().getConfigs().get(i).getUnit().getName();
                        String interval = setToMillisecond(value , timeType);

                        String key;

                        if(resConfig.getData().getConfigs().get(i).getName().toLowerCase().contains("page")){
                            key = PAGE_INTERVAL;
                        }else{
                            key = JOURNEY_INTERVAL;
                        }

                        Log.d(TAG, "doInBackground:name : "+resConfig.getData().getConfigs().get(i).getName()+" save interval by key "+key+" ,value : "+interval);

                        editor.putInt(key , Integer.valueOf(interval));
                        editor.commit();
                    }
                }

            }catch (IOException e){
                e.printStackTrace();
            }catch (Exception e){
                e.printStackTrace();
            }

            return null;
        }

        private String setToMillisecond(String value , String timeType) {

            String result = "0";

            if(timeType != null || !timeType.isEmpty()){

                if(timeType.equals(HOUR)){
                    int m = Integer.valueOf(value) * 3600000;
                    result = String.valueOf(m);
                }else if(timeType.equals(MINUTE)){
                    int m = Integer.valueOf(value) * 60000;
                    result = String.valueOf(m);

                }else {
                    int m = Integer.valueOf(value) * 1000;
                    result = String.valueOf(m);
                }
            }
            return result;
        }
    }
}
