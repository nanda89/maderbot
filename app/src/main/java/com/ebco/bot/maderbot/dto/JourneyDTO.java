package com.ebco.bot.maderbot.dto;

public class JourneyDTO extends DTO {

	private Long masterId;
	private Long monitorJourneyId = 0L;
	private long startTime;
	private String location;
	private String operator;
	private String name;
	private int cellId;
	private Double latitude;
	private Double longitude;
	private String networkType;
	private double signalLevel;
	private long responseTime;
	private int status;
	private double nvtSignal;
	private long nvtResponseTime;

	public Long getMasterId() {
		return masterId;
	}

	public void setMasterId(Long masterId) {
		this.masterId = masterId;
	}

	public Long getMonitorJourneyId() {
		return monitorJourneyId;
	}

	public void setMonitorJourneyId(Long monitorJourneyId) {
		this.monitorJourneyId = monitorJourneyId;
	}

	public long getStartTime() {
		return startTime;
	}

	public void setStartTime(long startTime) {
		this.startTime = startTime;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getOperator() {
		return operator;
	}

	public void setOperator(String operator) {
		this.operator = operator;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getCellId() {
		return cellId;
	}

	public void setCellId(int cellId) {
		this.cellId = cellId;
	}

	public Double getLatitude() {
		return latitude;
	}

	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}

	public Double getLongitude() {
		return longitude;
	}

	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}

	public String getNetworkType() {
		return networkType;
	}

	public void setNetworkType(String networkType) {
		this.networkType = networkType;
	}

	public double getSignalLevel() {
		return signalLevel;
	}

	public void setSignalLevel(double signalLevel) {
		this.signalLevel = signalLevel;
	}

	public long getResponseTime() {
		return responseTime;
	}

	public void setResponseTime(long responseTime) {
		this.responseTime = responseTime;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public double getNvtSignal() {
		return nvtSignal;
	}

	public void setNvtSignal(double nvtSignal) {
		this.nvtSignal = nvtSignal;
	}

	public long getNvtResponseTime() {
		return nvtResponseTime;
	}

	public void setNvtResponseTime(long nvtResponseTime) {
		this.nvtResponseTime = nvtResponseTime;
	}
}
