package com.ebco.bot.maderbot.activity;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Environment;
import android.util.Log;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.FileProvider;
import androidx.test.uiautomator.v18.BuildConfig;

import com.ebco.bot.maderbot.R;
import com.ebco.bot.maderbot.db.DatabaseApp;
import com.ebco.bot.maderbot.model.Journey;
import com.ebco.bot.maderbot.model.JourneyDetail;
import com.ebco.bot.maderbot.model.JourneyNVT;
import com.ebco.bot.maderbot.service.SendDataService;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Method;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import jxl.Workbook;
import jxl.WorkbookSettings;
import jxl.write.Label;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;

public class DownTimeActivity extends AppCompatActivity {

    private long timeCountInMilliSeconds = 60000;
    private static String TAG = "DOWNTIME";
    private static final String MyPREFERENCES = "MyPref";

    private enum TimerStatus {
        STARTED,
        STOPPED
    }

    private TimerStatus timerStatus = TimerStatus.STOPPED;
    private ProgressBar progressBarCircle;
    private EditText editTextMinute;
    private TextView textViewTime;
    private TextView tvVersion;
    private CountDownTimer countDownTimer;
    private File directory;
    private String filename;
    private static final String CODE_EXPORT_DATA_FAIL = "01";
    private static final String CODE_CLEANSING_DATA = "02";

    ProgressDialog bar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_down_time);

        Context context = getApplicationContext();
        initViews();

        @SuppressLint("SimpleDateFormat") SimpleDateFormat timeFormat = new SimpleDateFormat("mm");
        String m = timeFormat.format(new Date());
        int minute = Integer.valueOf(m);
        int mnt = 0;

        if (minute < 10) {
            mnt = 10 - minute;
        } else if (minute < 20) {
            mnt = 20 - minute;
        } else if (minute < 30) {
            mnt = 30 - minute;
        } else if (minute < 40) {
            mnt = 40 - minute;
        } else if (minute < 50) {
            mnt = 50 - minute;
        } else if (minute < 60) {
            mnt = 60 - minute;
        }
        saveDataToPreference(context);
//        enableHotspot(context);
        /*
            initialization file path and name file
         */
        File file = Environment.getExternalStorageDirectory();
        directory = new File(file.getAbsoluteFile() + "/QBOOSTR/DATA");
        if (!directory.exists()) {
            directory.mkdirs();
            Log.d(TAG, "onCreate: folder suc created");
        }
        @SuppressLint("SimpleDateFormat") SimpleDateFormat f =
                new SimpleDateFormat("yyyyMMddhhmmss");
        String date = f.format(new Date());
        filename = date + "_Data.xls";


        Log.d(TAG, "onCreate: filename : " + filename);

        editTextMinute.setEnabled(false);
        editTextMinute.setText(String.valueOf(mnt));
        startStop();

        try {
            PackageInfo info = context.getPackageManager().getPackageInfo(getPackageName(), 0);
            String ver = info.versionName;
            tvVersion.setText(String.format("Version : %s", ver));
            Log.d(TAG, "onCreate: version : " + ver);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
    }

    private void initViews() {
        progressBarCircle = findViewById(R.id.progressBarCircle);
        editTextMinute = findViewById(R.id.editTextMinute);
        textViewTime = findViewById(R.id.textViewTime);
        tvVersion = findViewById(R.id.tvVersion);

        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.inflateMenu(R.menu.main_menu);
        toolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem menuItem) {
                if (menuItem.getItemId() == R.id.db_config) {
                    goToConfig();
                } else if (menuItem.getItemId() == R.id.db_export) {
                    doExport();
                } else if (menuItem.getItemId() == R.id.db_clean) {
                    cleanData();
                } else if (menuItem.getItemId() == R.id.downloadApp) {
                    downloadApp();
                } else if (menuItem.getItemId() == R.id.downloadBot) {
                    downloadBot();
                } else if (menuItem.getItemId() == R.id.sendData) {
                    startService(new Intent(getApplicationContext(), SendDataService.class));
                }
                return false;
            }

        });
    }

    private void saveDataToPreference(Context context) {
        SharedPreferences prefs = context.getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();

        String urlServer = prefs.getString("url", "");
        assert urlServer != null;
        if (urlServer.isEmpty()) {
            urlServer = "http://192.168.43.23:8080";
            editor.putString("url", urlServer);
        }

        String tokenServer = prefs.getString("token", "");
        assert tokenServer != null;
        if (tokenServer.isEmpty()) {
            tokenServer = "1A107A527D70890A9E5E8E1FFD65DEA2";
            editor.putString("token", tokenServer);
        }


        editor.apply();
    }

    private void doExport() {
        Log.d(TAG, "doExport: process create workbook");
        Log.d(TAG, "doExport: filename : " + filename + " , directory : " + directory);
        File fileExport = new File(directory, filename);
        WorkbookSettings wbSetting = new WorkbookSettings();
        wbSetting.setLocale(new Locale("en", "EN"));
        WritableWorkbook workbook;

        try {
//            List<Journey> journeyList = DatabaseApp.getDatabaseApp(getApplicationContext()).journeyDao().getAllJourney();
//            List<JourneyDetail>journeyDetailList = DatabaseApp.getDatabaseApp(getApplicationContext()).journeyDetailDao().getAllJourneyDetail();
            List<Journey> journeyList =
                    DatabaseApp.getDatabaseApp(getApplicationContext()).journeyDao().getJourneyNoSent(false);
            List<JourneyDetail> journeyDetailList =
                    DatabaseApp.getDatabaseApp(getApplicationContext()).journeyDetailDao().getJourneyNoSent(false);

            if (journeyList.size() > 0 || journeyDetailList.size() > 0) {
                workbook = Workbook.createWorkbook(fileExport, wbSetting);
                writeJourney(workbook, CODE_EXPORT_DATA_FAIL);
                writeDetail(workbook, CODE_EXPORT_DATA_FAIL);
                writeNVT(workbook, CODE_EXPORT_DATA_FAIL);
                workbook.write();
                workbook.close();
            } else {
                Toast.makeText(DownTimeActivity.this, "Tidak ada data yang gagal kirim.", Toast.LENGTH_LONG).show();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void downloadApp() {
        new DownloadNewVersion().execute();
    }

    @SuppressLint("StaticFieldLeak")
    class DownloadNewVersion extends AsyncTask<String, Integer, Boolean> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            bar = new ProgressDialog(DownTimeActivity.this);
            bar.setCancelable(false);

            bar.setMessage("Downloading...");

            bar.setIndeterminate(true);
            bar.setCanceledOnTouchOutside(false);
            bar.show();
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            super.onProgressUpdate(values);

            bar.setIndeterminate(false);
            bar.setMax(100);
            bar.setProgress(values[0]);
            String msg;
            if (values[0] > 99) {
                msg = "Finishing...";
            } else {
                msg = "Downloading..." + values[0] + " %";
            }
            bar.setMessage(msg);
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);
            bar.dismiss();

            if (aBoolean) {
                Toast.makeText(DownTimeActivity.this, "Download successfully", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(DownTimeActivity.this, "Error : try again or check configuration for URL Application", Toast.LENGTH_SHORT).show();
            }
        }

        @Override
        protected Boolean doInBackground(String... strings) {
            boolean flag = false;

            SharedPreferences sharedPreferences =
                    getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
            String fileImagePath = sharedPreferences.getString("downloadApp", null);
            try {

                if (fileImagePath.isEmpty() || fileImagePath == null) {
                    throw new Exception("URL cannot be empty");
                }
                URL url = new URL(fileImagePath);
                HttpURLConnection c = (HttpURLConnection) url.openConnection();
                c.setRequestMethod("GET");
                c.connect();

                String PATH = Environment.getExternalStorageDirectory() + "/Download/";
                File file = new File(PATH);
                file.mkdirs();
                Log.d(TAG, "doInBackground: PATH : " + PATH + " ,is exist : " + file.exists());

                File outputFile = new File(file, "jenius.apk");
                if (outputFile.exists()) {
                    Log.d(TAG, "doInBackground: delete");
                    outputFile.delete();
                }

                InputStream is = c.getInputStream();
                int totalSize = 1431692;
                byte[] buffer = new byte[1024];
                int len1 = 0;
                int per = 0;
                int downloaded = 0;

                FileOutputStream fos = new FileOutputStream(outputFile);
                while ((len1 = is.read(buffer)) != -1) {
                    Log.d(TAG, "doInBackground: len1 : " + len1);

                    fos.write(buffer, 0, len1);
                    downloaded += len1;
                    Log.d(TAG, "doInBackground: download : " + downloaded);
                    per = (int) (downloaded * 100 / totalSize);
                    publishProgress(per);
                }
                fos.close();
                is.close();
                Log.d(TAG, "doInBackground: before run OpenNewVersion function");
//                OpenNewVersion(PATH , DownTimeActivity.this);

                flag = true;

            } catch (IndexOutOfBoundsException e) {
                e.printStackTrace();
                flag = false;
            } catch (Exception e) {
                e.printStackTrace();
                flag = false;
            }

            return flag;
        }

        private void OpenNewVersion(String path, Context context) {
            Log.d(TAG, "OpenNewVersion: path --> " + path);
            File directory = Environment.getExternalStoragePublicDirectory(path);
            File file =
                    new File(directory, "app-debug.apk"); // assume refers to "sdcard/myapp_folder/myapp.apk"
            Uri fileUri = Uri.fromFile(file); //for Build.VERSION.SDK_INT <= 24
            if (Build.VERSION.SDK_INT >= 24) {

                fileUri =
                        FileProvider.getUriForFile(context, BuildConfig.APPLICATION_ID + ".provider", file);
            }

            Intent intent = new Intent(Intent.ACTION_VIEW, fileUri);
            intent.putExtra(Intent.EXTRA_NOT_UNKNOWN_SOURCE, true);
            intent.setDataAndType(fileUri, "application/vnd.android.package-archive");
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION); //dont forget add this line
            context.startActivity(intent);
        }
    }

    private void downloadBot() {
        new DownloadNewVersionBot().execute();
    }

    @SuppressLint("StaticFieldLeak")
    class DownloadNewVersionBot extends AsyncTask<String, Integer, Boolean> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            bar = new ProgressDialog(DownTimeActivity.this);
            bar.setCancelable(false);

            bar.setMessage("Downloading...");

            bar.setIndeterminate(true);
            bar.setCanceledOnTouchOutside(false);
            bar.show();
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            super.onProgressUpdate(values);

            bar.setIndeterminate(false);
            bar.setMax(100);
            bar.setProgress(values[0]);
            String msg;
            if (values[0] > 99) {
                msg = "Finishing...";
            } else {
                msg = "Downloading..." + values[0] + " %";
            }
            bar.setMessage(msg);
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);
            bar.dismiss();

            if (aBoolean) {
                Toast.makeText(DownTimeActivity.this, "Download successfully", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(DownTimeActivity.this, "Error : try again or check configuration for URL Application", Toast.LENGTH_SHORT).show();
            }
        }

        @Override
        protected Boolean doInBackground(String... strings) {
            boolean flag = false;

            SharedPreferences sharedPreferences =
                    getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
            String fileImagePath = sharedPreferences.getString("downloadBot", null);
            Log.d(TAG, "doInBackground: URL ==>> " + fileImagePath);
            try {

                if (fileImagePath.isEmpty() || fileImagePath == null) {
                    throw new Exception("URL cannot be empty");
                }
                URL url = new URL(fileImagePath);
                HttpURLConnection c = (HttpURLConnection) url.openConnection();
                c.setRequestMethod("GET");
                c.connect();

                String PATH = Environment.getExternalStorageDirectory() + "/Download/";
                File file = new File(PATH);
                if (!file.exists())
                    file.mkdirs();
                Log.d(TAG, "doInBackground: PATH : " + PATH + " ,is exist : " + file.exists());

                File outputFile = new File(file, "jeniusBot.apk");
                if (outputFile.exists()) {
                    outputFile.delete();
                }

                InputStream is = c.getInputStream();
                int totalSize = 1431692;
                byte[] buffer = new byte[1024];
                int len1 = 0;
                int per = 0;
                int downloaded = 0;

                FileOutputStream fos = new FileOutputStream(outputFile);
                while ((len1 = is.read(buffer)) != -1) {
                    Log.d(TAG, "doInBackground: len1 : " + len1);

                    fos.write(buffer, 0, len1);
                    downloaded += len1;
                    Log.d(TAG, "doInBackground: download : " + downloaded);
                    per = (int) (downloaded * 100 / totalSize);
                    publishProgress(per);
                }
                fos.close();
                is.close();
                Log.d(TAG, "doInBackground: before run OpenNewVersion function");
//                OpenNewVersion(PATH , DownTimeActivity.this);

                flag = true;

            } catch (IndexOutOfBoundsException e) {
                e.printStackTrace();
                flag = false;
            } catch (Exception e) {
                e.printStackTrace();
                flag = false;
            }

            return flag;
        }

        private void OpenNewVersion(String path, Context context) {
            Log.d(TAG, "OpenNewVersion: path --> " + path);
//            Intent intent = new Intent(Intent.ACTION_VIEW);
//            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
//            intent.setDataAndType(Uri.fromFile(new File(path+"app-debug.apk")),
//                    "application/vnd.android.package-archive");
//            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//            startActivity(intent);

            File directory = Environment.getExternalStoragePublicDirectory(path);
            File file =
                    new File(directory, "app-debug.apk"); // assume refers to "sdcard/myapp_folder/myapp.apk"
            Uri fileUri = Uri.fromFile(file); //for Build.VERSION.SDK_INT <= 24
            if (Build.VERSION.SDK_INT >= 24) {

                fileUri =
                        FileProvider.getUriForFile(context, BuildConfig.APPLICATION_ID + ".provider", file);
            }

            Intent intent = new Intent(Intent.ACTION_VIEW, fileUri);
            intent.putExtra(Intent.EXTRA_NOT_UNKNOWN_SOURCE, true);
            intent.setDataAndType(fileUri, "application/vnd.android.package-archive");
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION); //dont forget add this line
            context.startActivity(intent);
        }
    }

    private void cleanData() {
        File file = Environment.getExternalStorageDirectory();
        File dirCleansing = new File(file.getAbsoluteFile() + "/BTPN/Cleansing");
        if (!dirCleansing.exists()) {
            dirCleansing.mkdirs();
        }
        File fileExport;
        fileExport = new File(dirCleansing, filename);
        WorkbookSettings wbSetting = new WorkbookSettings();
        wbSetting.setLocale(new Locale("en", "EN"));
        WritableWorkbook workbook;
        List<Journey> journeyList =
                DatabaseApp.getDatabaseApp(getApplicationContext()).journeyDao().getAllJourney();
        List<JourneyDetail> journeyDetailList =
                DatabaseApp.getDatabaseApp(getApplicationContext()).journeyDetailDao().getAllJourneyDetail();
        if (journeyList.size() > 0 || journeyDetailList.size() > 0) {
            try {
                workbook = Workbook.createWorkbook(fileExport, wbSetting);
                writeJourney(workbook, CODE_CLEANSING_DATA);
                writeDetail(workbook, CODE_CLEANSING_DATA);
                writeNVT(workbook, CODE_CLEANSING_DATA);
                workbook.write();
                workbook.close();

                deleteAllTable();

            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            Toast.makeText(DownTimeActivity.this, "Tidak ditemukan data pada database.", Toast.LENGTH_LONG).show();
        }

    }
    private void deleteAllTable() {
        DatabaseApp.getDatabaseApp(getApplicationContext()).journeyDao().deleteAll();
        DatabaseApp.getDatabaseApp(getApplicationContext()).journeyDetailDao().deleteAll();
        DatabaseApp.getDatabaseApp(getApplicationContext()).journeyNVTDao().deleteAll();
        DatabaseApp.getDatabaseApp(getApplicationContext()).journeyErrorDao().deleteAll();
    }
    private void goToConfig() {
        Intent intent = new Intent(DownTimeActivity.this, ConfigActivity.class);
        startActivity(intent);
    }
    /*
        code 01     => for export data fail
        code 02     => for export data all in database(cleansing database)
     */
    private void writeJourney(WritableWorkbook workbook, String code) {
        WritableSheet sheet = workbook.createSheet("Journey", 0);
        try {
            sheet.addCell(new Label(0, 0, "ID"));
            sheet.addCell(new Label(1, 0, "TOKEN"));
            sheet.addCell(new Label(2, 0, "CELL ID"));
            sheet.addCell(new Label(3, 0, "JOURNEY MASTER ID"));
            sheet.addCell(new Label(4, 0, "LATITUDE"));
            sheet.addCell(new Label(5, 0, "LONGITUDE"));
            sheet.addCell(new Label(6, 0, "IS SENT"));
            sheet.addCell(new Label(7, 0, "TYPE"));
            sheet.addCell(new Label(8, 0, "DATETIME"));

            List<Journey> journeyList;
            if (code.equals(CODE_EXPORT_DATA_FAIL)) {
                journeyList =
                        DatabaseApp.getDatabaseApp(getApplicationContext()).journeyDao().getJourneyNoSent(false);
                Log.d(TAG, "doExport: export data fail, total journey " + journeyList.size() + " rows ");
            } else {
                journeyList =
                        DatabaseApp.getDatabaseApp(getApplicationContext()).journeyDao().getAllJourney();
                Log.d(TAG, "doExport: cleansing data ,total journey " + journeyList.size() + " rows ");
            }
            if (journeyList.size() > 0) {
                int index = 1;
                for (Journey journey : journeyList) {
                    Log.d(TAG, "doExport: process init data into excel, id " + journey.getId());
                    sheet.addCell(new Label(0, index, String.valueOf(journey.getId())));
                    sheet.addCell(new Label(1, index, journey.getToken()));
                    sheet.addCell(new Label(2, index, String.valueOf(journey.getCellId())));
                    sheet.addCell(new Label(3, index, journey.getJourney_master_id()));
                    sheet.addCell(new Label(4, index, journey.getLatitude()));
                    sheet.addCell(new Label(5, index, journey.getLongitude()));

                    String sent = "Terkirim";
                    if (!journey.isIs_sent()) {
                        sent = "Tidak Terkirim";
                    }
                    sheet.addCell(new Label(6, index, sent));
                    sheet.addCell(new Label(7, index, journey.getType()));
                    sheet.addCell(new Label(8, index, journey.getDatetime()));

                    index += 1;
                }

                Log.d(TAG, "doExport: write data into excel");
                Toast.makeText(DownTimeActivity.this, "Databerhasil di export, nama file " + filename, Toast.LENGTH_LONG).show();
            }

        } catch (WriteException e) {
            e.printStackTrace();
        }

    }

    private void writeDetail(WritableWorkbook workbook, String code) {
        WritableSheet sheet = workbook.createSheet("Detail", 1);
        try {
            sheet.addCell(new Label(0, 0, "ID"));
            sheet.addCell(new Label(1, 0, "TOKEN"));
            sheet.addCell(new Label(2, 0, "MONITOR ID(JOURNEY ID)"));
            sheet.addCell(new Label(3, 0, "JOURNEY DETAIL MASTER ID"));
            sheet.addCell(new Label(4, 0, "CELL ID"));
            sheet.addCell(new Label(5, 0, "SIGNAL LEVEL"));
            sheet.addCell(new Label(6, 0, "NETWORK TYPE"));
            sheet.addCell(new Label(7, 0, "COMMAND"));
            sheet.addCell(new Label(8, 0, "STATUS"));
            sheet.addCell(new Label(9, 0, "MESSAGE"));
            sheet.addCell(new Label(10, 0, "DATETIME"));
            sheet.addCell(new Label(11, 0, "RESPONSE TIME"));
            sheet.addCell(new Label(12, 0, "REPEAT NO"));
            sheet.addCell(new Label(13, 0, "IS SENT"));
            sheet.addCell(new Label(13, 0, "JOURNEY ID"));

            List<JourneyDetail> journeyDetailList;
            if (code.equals(CODE_EXPORT_DATA_FAIL)) {
                journeyDetailList =
                        DatabaseApp.getDatabaseApp(getApplicationContext()).journeyDetailDao().getJourneyNoSentAll(false);
                Log.d(TAG, "doExport:export data, total journey " + journeyDetailList.size() + " rows ");
            } else {
                journeyDetailList =
                        DatabaseApp.getDatabaseApp(getApplicationContext()).journeyDetailDao().getAllJourneyDetail();
                Log.d(TAG, "doExport:cleansing data, total journey " + journeyDetailList.size() + " rows ");
            }

            if (journeyDetailList.size() > 0) {
                int index = 1;
                for (JourneyDetail journeyDetail : journeyDetailList) {

                    String sent = "Terkirim";

                    if (!journeyDetail.isSent()) {
                        sent = "Tidak Terkirim";
                    }

                    Log.d(TAG, "doExport: process init data into excel, monitoring id " + journeyDetail.getMonitor_journey_id());
                    sheet.addCell(new Label(0, index, String.valueOf(journeyDetail.getId())));
                    sheet.addCell(new Label(1, index, journeyDetail.getToken()));
                    String monitoring_id = "0";
                    if (journeyDetail.getMonitor_journey_id() != null) {
                        monitoring_id = journeyDetail.getMonitor_journey_id().toString();
                    }
                    sheet.addCell(new Label(2, index, monitoring_id));
                    sheet.addCell(new Label(3, index, journeyDetail.getJourney_detail_id()));
                    sheet.addCell(new Label(4, index, String.valueOf(journeyDetail.getCellid())));
                    sheet.addCell(new Label(5, index, String.valueOf(journeyDetail.getSignal_level())));
                    sheet.addCell(new Label(6, index, journeyDetail.getNetwork_type()));
                    sheet.addCell(new Label(7, index, journeyDetail.getCommand()));
                    sheet.addCell(new Label(8, index, String.valueOf(journeyDetail.getStatus())));
                    sheet.addCell(new Label(9, index, journeyDetail.getMessage()));
                    sheet.addCell(new Label(10, index, journeyDetail.getDatetime()));
                    sheet.addCell(new Label(11, index, journeyDetail.getResponse_time()));
                    sheet.addCell(new Label(12, index, String.valueOf(journeyDetail.getRepeat_no())));
                    sheet.addCell(new Label(13, index, sent));
                    sheet.addCell(new Label(13, index, String.valueOf(journeyDetail.getJourneyid())));
                    index += 1;
                }

                Log.d(TAG, "doExport: write data into excel");
                Toast.makeText(DownTimeActivity.this, "Databerhasil di export, nama file " + filename, Toast.LENGTH_LONG).show();
            }

        } catch (WriteException e) {
            e.printStackTrace();
        }
    }
    private void writeNVT(WritableWorkbook workbook, String code) {
        WritableSheet sheet = workbook.createSheet("NVT", 2);
        try {
            sheet.addCell(new Label(0, 0, "ID"));
            sheet.addCell(new Label(1, 0, "TOKEN"));
            sheet.addCell(new Label(2, 0, "MONITOR ID"));
            sheet.addCell(new Label(3, 0, "CELL ID"));
            sheet.addCell(new Label(4, 0, "SIGNAL LEVEL"));
            sheet.addCell(new Label(5, 0, "NETWORK TYPE"));
            sheet.addCell(new Label(6, 0, "COMMAND"));
            sheet.addCell(new Label(7, 0, "STATUS"));
            sheet.addCell(new Label(8, 0, "MESSAGE"));
            sheet.addCell(new Label(9, 0, "DATETIME"));
            sheet.addCell(new Label(10, 0, "RESPONSE TIME"));
            sheet.addCell(new Label(11, 0, "REPEAT NO"));
            sheet.addCell(new Label(12, 0, "IS SENT"));
            sheet.addCell(new Label(13, 0, "JOURNEY ID"));

            sheet.addCell(new Label(14, 0, "OPERATOR"));
            sheet.addCell(new Label(15, 0, "ARFN"));
            sheet.addCell(new Label(16, 0, "BANDWITH"));
            sheet.addCell(new Label(17, 0, "SIGNAL STRENGTH"));
            sheet.addCell(new Label(18, 0, "SIGNAL QUALITY"));
            sheet.addCell(new Label(19, 0, "SNNR"));
            sheet.addCell(new Label(20, 0, "TIMING ADVANCE"));
            sheet.addCell(new Label(21, 0, "CQI"));
            sheet.addCell(new Label(22, 0, "SIGNAL INDEX"));

            List<JourneyNVT> nvtList;
            if (code.equals(CODE_EXPORT_DATA_FAIL)) {
                nvtList =
                        DatabaseApp.getDatabaseApp(getApplicationContext()).journeyNVTDao().getNVTNoSent(false);
                Log.d(TAG, "doExport:export data, total journey " + nvtList.size() + " rows ");
            } else {
                nvtList =
                        DatabaseApp.getDatabaseApp(getApplicationContext()).journeyNVTDao().getAllNVT();
                Log.d(TAG, "doExport:cleansing data, total journey " + nvtList.size() + " rows ");
            }

            if (nvtList.size() > 0) {
                int index = 1;
                for (JourneyNVT nvt : nvtList) {

                    String sent = "Terkirim";

                    if (!nvt.isSent()) {
                        sent = "Tidak Terkirim";
                    }

                    Log.d(TAG, "doExport: process init data into excel, id " + nvt.getId());
                    sheet.addCell(new Label(0, index, String.valueOf(nvt.getId())));
                    sheet.addCell(new Label(1, index, nvt.getToken()));
                    sheet.addCell(new Label(2, index, nvt.getMonitor_journey_id()));
                    sheet.addCell(new Label(3, index, String.valueOf(nvt.getCellid())));
                    sheet.addCell(new Label(4, index, String.valueOf(nvt.getSignal_level())));
                    sheet.addCell(new Label(5, index, nvt.getNetwork_type()));
                    sheet.addCell(new Label(6, index, nvt.getCommand()));
                    sheet.addCell(new Label(7, index, String.valueOf(nvt.getStatus())));
                    sheet.addCell(new Label(8, index, nvt.getMessage()));
                    sheet.addCell(new Label(9, index, nvt.getDatetime()));
                    sheet.addCell(new Label(10, index, nvt.getResponse_time()));
                    sheet.addCell(new Label(11, index, String.valueOf(nvt.getRepeat_no())));
                    sheet.addCell(new Label(12, index, sent));
                    sheet.addCell(new Label(13, index, String.valueOf(nvt.getJourneyid())));

                    sheet.addCell(new Label(14, index, nvt.getOperator()));
                    sheet.addCell(new Label(15, index, String.valueOf(nvt.getUarfn())));
                    sheet.addCell(new Label(16, index, String.valueOf(nvt.getBandwith())));
                    sheet.addCell(new Label(17, index, String.valueOf(nvt.getSignalStrength())));
                    sheet.addCell(new Label(18, index, String.valueOf(nvt.getSignalQuality())));
                    sheet.addCell(new Label(19, index, String.valueOf(nvt.getSnnr())));
                    sheet.addCell(new Label(20, index, String.valueOf(nvt.getTimingAdvance())));
                    sheet.addCell(new Label(21, index, String.valueOf(nvt.getCqi())));
                    sheet.addCell(new Label(22, index, String.valueOf(nvt.getSignalIndex())));
                    index += 1;
                }

                Log.d(TAG, "doExport: write data into excel");
                Toast.makeText(DownTimeActivity.this, "Databerhasil di export, nama file " + filename, Toast.LENGTH_LONG).show();
            }

        } catch (WriteException e) {
            e.printStackTrace();
        }
    }
    /**
     * method to start and stop count down timer
     */
    private void startStop() {
        if (timerStatus == TimerStatus.STOPPED) {

            // call to initialize the timer values
            setTimerValues();
            // call to initialize the progress bar values
            setProgressBarValues();
            editTextMinute.setEnabled(false);
            // changing the timer status to started
            timerStatus = TimerStatus.STARTED;
            startCountDownTimer();

        } else {
            editTextMinute.setEnabled(false);
            // changing the timer status to stopped
            timerStatus = TimerStatus.STOPPED;
            stopCountDownTimer();

        }

    }
    /**
     * method to initialize the values for count down timer
     */
    private void setTimerValues() {
        int time = 0;
        if (!editTextMinute.getText().toString().isEmpty()) {
            // fetching value from edit text and type cast to integer
            time = Integer.parseInt(editTextMinute.getText().toString().trim());
        } else {
            // toast message to fill edit text
            Toast.makeText(getApplicationContext(), getString(R.string.message_minutes), Toast.LENGTH_LONG).show();
        }
        // assigning values after converting to milliseconds
        timeCountInMilliSeconds = time * 60 * 1000;
    }

    /**
     * method to start count down timer
     */
    private void startCountDownTimer() {

        countDownTimer = new CountDownTimer(timeCountInMilliSeconds, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {

                textViewTime.setText(hmsTimeFormatter(millisUntilFinished));

                progressBarCircle.setProgress((int) (millisUntilFinished / 1000));

            }

            @Override
            public void onFinish() {

                textViewTime.setText(hmsTimeFormatter(timeCountInMilliSeconds));
                // call to initialize the progress bar values
                setProgressBarValues();
                editTextMinute.setEnabled(true);
                // changing the timer status to stopped
                timerStatus = TimerStatus.STOPPED;
            }

        }.start();
        countDownTimer.start();
    }

    /**
     * method to stop count down timer
     */
    private void stopCountDownTimer() {
        countDownTimer.cancel();
    }

    /**
     * method to set circular progress bar values
     */
    private void setProgressBarValues() {

        progressBarCircle.setMax((int) timeCountInMilliSeconds / 1000);
        progressBarCircle.setProgress((int) timeCountInMilliSeconds / 1000);
    }
    /**
     * method to convert millisecond to time format
     *
     * @param milliSeconds
     * @return HH:mm:ss time formatted string
     */
    @SuppressLint("DefaultLocale")
    private String hmsTimeFormatter(long milliSeconds) {

        return String.format("%02d:%02d:%02d",
                TimeUnit.MILLISECONDS.toHours(milliSeconds),
                TimeUnit.MILLISECONDS.toMinutes(milliSeconds) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(milliSeconds)),
                TimeUnit.MILLISECONDS.toSeconds(milliSeconds) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(milliSeconds)));

    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    private void enableHotspot(Context context) {
        boolean hotspot = isApOn(context);
        Log.d(TAG, "enableHotspot: is hotspot is enable " + hotspot);
        if (!hotspot) {
            Log.d(TAG, "enableHotspot: hotspot is enable");
//            turnOnHotspot();
        }
    }

    public static boolean isApOn(Context context) {
        WifiManager wifimanager =
                (WifiManager) context.getApplicationContext().getSystemService(WIFI_SERVICE);
        try {
            @SuppressLint("PrivateApi") Method method =
                    wifimanager.getClass().getDeclaredMethod("isWifiApEnabled");
            method.setAccessible(true);
            return (Boolean) method.invoke(wifimanager);
        } catch (Throwable ignored) {
        }
        return false;
    }



}