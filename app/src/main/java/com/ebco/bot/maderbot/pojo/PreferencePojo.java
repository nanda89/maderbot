package com.ebco.bot.maderbot.pojo;

public class PreferencePojo {

    private String token;
    private String deviceId;
    private String journeyId;

    private String pageStart;
    private String pageLogin;
    private String pageAccount;

    // tabungan page
    private String pageSaving;
    // transfer dana page
    private String pageTransfer;
    // transfer page
    private String pageDestAccount;
    // pindah buku page
    private String pageFormTransfer;

    private String pagePurchase;
    private String pagePulse;
    private String pageProviderList;
    private String pageProvider;
//    private String pagePurchaseForm;

    public String getPageFormTransfer() {
        return pageFormTransfer;
    }

    public void setPageFormTransfer(String pageFormTransfer) {
        this.pageFormTransfer = pageFormTransfer;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public String getJourneyId() {
        return journeyId;
    }

    public void setJourneyId(String journeyId) {
        this.journeyId = journeyId;
    }

    public String getPageStart() {
        return pageStart;
    }

    public void setPageStart(String pageStart) {
        this.pageStart = pageStart;
    }

    public String getPageLogin() {
        return pageLogin;
    }

    public void setPageLogin(String pageLogin) {
        this.pageLogin = pageLogin;
    }

    public String getPageAccount() {
        return pageAccount;
    }

    public void setPageAccount(String pageAccount) {
        this.pageAccount = pageAccount;
    }

    public String getPageSaving() {
        return pageSaving;
    }

    public void setPageSaving(String pageSaving) {
        this.pageSaving = pageSaving;
    }

    public String getPageTransfer() {
        return pageTransfer;
    }

    public void setPageTransfer(String pageTransfer) {
        this.pageTransfer = pageTransfer;
    }

    public String getPageDestAccount() {
        return pageDestAccount;
    }

    public void setPageDestAccount(String pageDestAccount) {
        this.pageDestAccount = pageDestAccount;
    }

    public String getPagePurchase() {
        return pagePurchase;
    }

    public void setPagePurchase(String pagePurchase) {
        this.pagePurchase = pagePurchase;
    }

    public String getPagePulse() {
        return pagePulse;
    }

    public void setPagePulse(String pagePulse) {
        this.pagePulse = pagePulse;
    }

    public String getPageProviderList() {
        return pageProviderList;
    }

    public void setPageProviderList(String pageProviderList) {
        this.pageProviderList = pageProviderList;
    }

    public String getPageProvider() {
        return pageProvider;
    }

    public void setPageProvider(String pageProvider) {
        this.pageProvider = pageProvider;
    }

//    public String getPagePurchaseForm() {
//        return pagePurchaseForm;
//    }
//
//    public void setPagePurchaseForm(String pagePurchaseForm) {
//        this.pagePurchaseForm = pagePurchaseForm;
//    }
}
