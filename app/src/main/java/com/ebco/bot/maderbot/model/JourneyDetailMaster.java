package com.ebco.bot.maderbot.model;

import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.Index;

@Entity(tableName = "journey_detail_master", foreignKeys = @ForeignKey(entity = JourneyMaster.class, parentColumns =
        "id", childColumns = "journeymasterid", onDelete = ForeignKey.CASCADE, onUpdate = ForeignKey
        .CASCADE), indices = @Index({"id", "journeymasterid"}))
public class JourneyDetailMaster extends ModelEntity {

    private String name;
    private Long journeymasterid;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getJourneymasterid() {
        return journeymasterid;
    }

    public void setJourneymasterid(Long journeymasterid) {
        this.journeymasterid = journeymasterid;
    }
}
