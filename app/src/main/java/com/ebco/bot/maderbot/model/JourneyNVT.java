package com.ebco.bot.maderbot.model;

import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.Index;

@Entity(tableName = "journey_nvt", foreignKeys = @ForeignKey(entity = Journey.class, parentColumns =
        "id", childColumns = "journeyid", onDelete = ForeignKey.CASCADE, onUpdate = ForeignKey
        .CASCADE), indices = @Index({"id", "journeyid"}))
public class JourneyNVT extends ModelEntity {

    /**
     * isError = true       it mean page error
     * isError = false      it mean page success
     *
     * -------------- status--------------
     * 0    => success
     * 1    => error
     */
    private String token;
    /*
        response from API journey
     */
    private String monitor_journey_id;

//    private String journey_detail_id;
    private String network_type;
    private int cellid;
    private double signal_level;
    private String response_time;
    private String command;
    private int status;
    private String message;
    private String datetime;
    private int repeat_no;
    private int percentage;

    /*
        new parameter
     */
    private String operator;
    private int uarfn;
    private int bandwith;
    private int cellBandwith;
    private int signalStrength;
    private int signalQuality;
    private int snnr;
    private int timingAdvance;
    private int cqi;
    private int SignalIndex;

    /*
        local id
     */
    private Long journeyid;
    private boolean isSent;
    private boolean isError;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getMonitor_journey_id() {
        return monitor_journey_id;
    }

    public void setMonitor_journey_id(String monitor_journey_id) {
        this.monitor_journey_id = monitor_journey_id;
    }

    public String getNetwork_type() {
        return network_type;
    }

    public void setNetwork_type(String network_type) {
        this.network_type = network_type;
    }

    public int getCellid() {
        return cellid;
    }

    public void setCellid(int cellid) {
        this.cellid = cellid;
    }

    public double getSignal_level() {
        return signal_level;
    }

    public void setSignal_level(double signal_level) {
        this.signal_level = signal_level;
    }

    public String getResponse_time() {
        return response_time;
    }

    public void setResponse_time(String response_time) {
        this.response_time = response_time;
    }

    public String getCommand() {
        return command;
    }

    public void setCommand(String command) {
        this.command = command;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getDatetime() {
        return datetime;
    }

    public void setDatetime(String datetime) {
        this.datetime = datetime;
    }

    public int getRepeat_no() {
        return repeat_no;
    }

    public void setRepeat_no(int repeat_no) {
        this.repeat_no = repeat_no;
    }

    public Long getJourneyid() {
        return journeyid;
    }

    public void setJourneyid(Long journeyid) {
        this.journeyid = journeyid;
    }

    public boolean isSent() {
        return isSent;
    }

    public void setSent(boolean sent) {
        isSent = sent;
    }

    public boolean isError() {
        return isError;
    }

    public void setError(boolean error) {
        isError = error;
    }

    public int getPercentage() {
        return percentage;
    }

    public void setPercentage(int percentage) {
        this.percentage = percentage;
    }

    public String getOperator() {
        return operator;
    }

    public void setOperator(String operator) {
        this.operator = operator;
    }

    public int getUarfn() {
        return uarfn;
    }

    public void setUarfn(int uarfn) {
        this.uarfn = uarfn;
    }

    public int getBandwith() {
        return bandwith;
    }

    public void setBandwith(int bandwith) {
        this.bandwith = bandwith;
    }

    public int getCellBandwith() {
        return cellBandwith;
    }

    public void setCellBandwith(int cellBandwith) {
        this.cellBandwith = cellBandwith;
    }

    public int getSignalStrength() {
        return signalStrength;
    }

    public void setSignalStrength(int signalStrength) {
        this.signalStrength = signalStrength;
    }

    public int getSignalQuality() {
        return signalQuality;
    }

    public void setSignalQuality(int signalQuality) {
        this.signalQuality = signalQuality;
    }

    public int getSnnr() {
        return snnr;
    }

    public void setSnnr(int snnr) {
        this.snnr = snnr;
    }

    public int getTimingAdvance() {
        return timingAdvance;
    }

    public void setTimingAdvance(int timingAdvance) {
        this.timingAdvance = timingAdvance;
    }

    public int getCqi() {
        return cqi;
    }

    public void setCqi(int cqi) {
        this.cqi = cqi;
    }

    public int getSignalIndex() {
        return SignalIndex;
    }

    public void setSignalIndex(int signalIndex) {
        SignalIndex = signalIndex;
    }
}
