package com.ebco.bot.maderbot;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.ebco.bot.maderbot.pojo.ResponseGeneral;
import com.google.gson.Gson;

import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;

public class SendData {
    public static final String TAG = "TEST-BOT";


    @SuppressLint("StaticFieldLeak")
    public static class submitTotalTimeCycle extends AsyncTask<Void, String, Boolean> {

        String urlServer;
        @SuppressLint("StaticFieldLeak")
        Context context;
        String token;
        double totalTime;


        submitTotalTimeCycle(String urlServer,double totalTime, Context context, String token) {
            super();
            this.urlServer = urlServer;
            this.totalTime = totalTime;
            this.context = context;
            this.token = token;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            Log.d(TAG, "onPreExecute: total cycle");

        }

        @Override
        protected Boolean doInBackground(Void... params) {

            boolean result = false;
            try {

                Log.d(TAG, "API : " + urlServer + "/api/v1/journeyCycle\n" +
                        "############ Parameter ############\n" +
                        "token : " + token + "\n" +
                        "total : " + totalTime
                );

                OkHttpClient client = new OkHttpClient();
                FormBody.Builder formParameter = new FormBody.Builder()
                        .add("total", String.valueOf(totalTime));

                RequestBody formBody = formParameter.build();
                Request request = new Request.Builder()
                        .addHeader("token", token)
                        .addHeader("Content-Type", "application/x-www-form-urlencoded")
                        .url(urlServer + "/api/v1/journeyCycle")
                        .post(formBody)
                        .build();

                okhttp3.Response response = client.newCall(request).execute();

                Gson gson = new Gson();
                assert response.body() != null;
                String jsonData = response.body().string();
                Log.d(TAG, "doInBackground: response cycle ---> " + jsonData);
                ResponseGeneral responseMessage = gson.fromJson(jsonData, ResponseGeneral.class);

                if (responseMessage.getResponseCode() == 200) {
                    result = true;
                }

            } catch (Exception e) {
                e.printStackTrace();
                result = false;
            }

            return result;
        }

        @Override
        protected void onPostExecute(Boolean response) {
            super.onPostExecute(response);
        }
    }
}
