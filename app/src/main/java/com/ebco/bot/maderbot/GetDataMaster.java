package com.ebco.bot.maderbot;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.util.Log;

import com.ebco.bot.maderbot.db.DatabaseApp;
import com.ebco.bot.maderbot.model.JourneyDetailMaster;
import com.ebco.bot.maderbot.model.JourneyMaster;
import com.ebco.bot.maderbot.pojo.ResponseJourneyDetailMaster;
import com.ebco.bot.maderbot.pojo.ResponseJourneyMaster;
import com.google.gson.Gson;

import java.io.IOException;
import java.util.List;

import okhttp3.OkHttpClient;
import okhttp3.Request;

public class GetDataMaster {

    private static final String TAG = "TEST-BOT";
    private static final String MyPREFERENCES = "MyPref";
    @SuppressLint("StaticFieldLeak")
    public static class getJourneyMaster extends AsyncTask<Void, String, Boolean> {

        String url;
        String token;
        Context context;
        getJourneyMaster(String url , String token , Context context){
            super();
            this.url = url;
            this.token = token;
            this.context = context;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Boolean doInBackground(Void... voids) {

            OkHttpClient client = new OkHttpClient();

            Request request = new Request.Builder()
                    .url(url + "/api/v1/journeyMaster")
                    .header("token", token)
                    .build();

            try {
                okhttp3.Response response = client.newCall(request)
                        .execute();

                String responseMessage = response.body().string();
                Log.d(TAG, "onResponse: response from journey master : " + responseMessage);
                Gson gson = new Gson();
                ResponseJourneyMaster resJourneyMaster = gson.fromJson(responseMessage, ResponseJourneyMaster.class);

                Log.d(TAG, "doInBackground: responseCode : "+resJourneyMaster.getResponseCode()+" , status : "+resJourneyMaster.isStatus());


                if (resJourneyMaster.getResponseCode() == 200) {
                    List<JourneyMaster> journeyMasters = DatabaseApp.getDatabaseApp(context).journeyMasterDao().getAll();
                    Log.d(TAG, "doInBackground: total journey master data : "+journeyMasters.size());

                    if (journeyMasters.size() > 0) {
                        //update
                        Log.d(TAG, "onResponse: proses update journey master");
                        for (JourneyMaster jm : resJourneyMaster.getData().getJourneyMasters()) {

                            if(jm != null){
                                DatabaseApp.getDatabaseApp(context).journeyMasterDao().update(jm);
                            }else{
                                DatabaseApp.getDatabaseApp(context).journeyMasterDao().insert(jm);
                            }

                        }
                    } else {
                        // insert
                        Log.d(TAG, "onResponse: proses insert journey master");
                        if (resJourneyMaster.getData().getJourneyMasters().size() > 0) {
                            for (JourneyMaster jm : resJourneyMaster.getData().getJourneyMasters()) {
                                Long id = jm.getId();
                                String name = jm.getName();
                                String type = jm.getType();
                                DatabaseService.insertJourneyMaster(id ,name, type, context);
                            }
                        }
                    }
                }

            }catch (IOException e){
                e.printStackTrace();
            }catch (Exception e){
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Boolean response) {
            super.onPostExecute(response);
        }
    }

    @SuppressLint("StaticFieldLeak")
    public static class getJourneyDetailMaster extends AsyncTask<Void, String, Boolean> {

        long journeyMasterId;
        String url;
        String token;
        Context context;

        getJourneyDetailMaster(long journeyMasterId , String url , String token , Context context){
            super();
            this.journeyMasterId = journeyMasterId;
            this.url = url;
            this.token = token;
            this.context = context;
        }
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Boolean doInBackground(Void... voids) {

            OkHttpClient client = new OkHttpClient();

            Request request = new Request.Builder()
                    .url(url + "/api/v1/journeyDetailMaster?journeyID="+journeyMasterId)
                    .header("token", token)
                    .build();
            Log.d(TAG, "doInBackground: token : "+token);
            Log.d(TAG, "doInBackground: URL detail master : "+url+"/api/v1/journeyDetailMaster?journeyID="+journeyMasterId);

            try {
                okhttp3.Response response = client.newCall(request)
                        .execute();
                String responseMessage = response.body().string();
                Log.d(TAG, "onResponse: response from journey detail master : " + responseMessage);
                Gson gson = new Gson();
                ResponseJourneyDetailMaster resJourneyDetailMaster = gson.fromJson(responseMessage, ResponseJourneyDetailMaster.class);
                Log.d(TAG, "doInBackground: response code from detail master : "+resJourneyDetailMaster.getResponseCode());

                if (resJourneyDetailMaster.getResponseCode() == 200) {
                    List<JourneyDetailMaster> journeyDetailMasters = DatabaseApp.getDatabaseApp(context).journeyDetailMasterDao().getListByJourneyId(journeyMasterId);

                    if (journeyDetailMasters.size() > 0) {
                        //update
                        Log.d(TAG, "onResponse: proses update journey detail master");
                        for(int i = 0 ; i < journeyDetailMasters.size();i++){
                            JourneyDetailMaster jdm = new JourneyDetailMaster();
                            jdm.setId(resJourneyDetailMaster.getData().getJourneyDetailMasters().get(i).getId());
                            jdm.setName(resJourneyDetailMaster.getData().getJourneyDetailMasters().get(i).getName());
                            jdm.setJourneymasterid(resJourneyDetailMaster.getData().getJourneyDetailMasters().get(i).getJourneyMaster().getId());

                            DatabaseApp.getDatabaseApp(context).journeyDetailMasterDao().update(jdm);
                        }

                    } else {
                        // insert
                        Log.d(TAG, "onResponse: proses insert journey master");
                        if (resJourneyDetailMaster.getData().getJourneyDetailMasters().size() > 0) {

                            for(int i = 0 ; i < resJourneyDetailMaster.getData().getJourneyDetailMasters().size();i++){
                                JourneyDetailMaster journeyDetailMaster = new JourneyDetailMaster();
                                journeyDetailMaster.setId(resJourneyDetailMaster.getData().getJourneyDetailMasters().get(i).getId());
                                journeyDetailMaster.setName(resJourneyDetailMaster.getData().getJourneyDetailMasters().get(i).getName());
                                journeyDetailMaster.setJourneymasterid(resJourneyDetailMaster.getData().getJourneyDetailMasters().get(i).getJourneyMaster().getId());

                                DatabaseApp.getDatabaseApp(context).journeyDetailMasterDao().insert(journeyDetailMaster);
                            }
                        }
                    }

                    updateSharedPreferences(resJourneyDetailMaster,context);

                }
            }catch (IOException e){
                e.printStackTrace();
            }catch (Exception e){
                e.printStackTrace();
            }

            return null;
        }

        private void updateSharedPreferences(ResponseJourneyDetailMaster responseJourneyDetailMaster, Context context) {

            for(int i = 0 ; i < responseJourneyDetailMaster.getData().getJourneyDetailMasters().size() ; i++){
                SharedPreferences sharedPreferences = context.getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPreferences.edit();
                String key = responseJourneyDetailMaster.getData().getJourneyDetailMasters().get(i).getName().replace(" " , "");
                String value = responseJourneyDetailMaster.getData().getJourneyDetailMasters().get(i).getId().toString();

                Log.d(TAG, "updateSharedPreferences: key : "+key+" , value : "+value);

                editor.putString(key , value);
                editor.commit();
            }

        }

        @Override
        protected void onPostExecute(Boolean response) {
            super.onPostExecute(response);
        }
    }
}
