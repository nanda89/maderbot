package com.ebco.bot.maderbot.dao;

import androidx.room.Dao;
import androidx.room.Query;

import com.ebco.bot.maderbot.model.JourneyDetailMaster;

import java.util.List;

@Dao
public abstract class JourneyDetailMasterDao implements BaseDao<JourneyDetailMaster> {

    @Query("SELECT * FROM journey_detail_master")
    public  abstract List<JourneyDetailMaster> getAll();

    @Query("SELECT * FROM journey_detail_master where id = :id")
    public abstract JourneyDetailMaster getById(Long id);

    @Query("SELECT * FROM journey_detail_master where name GLOB '*' || :name || '*' and journeymasterid = :journeyMasterId LIMIT 1")
    public abstract JourneyDetailMaster getContainName(String name , Long journeyMasterId);

    @Query("SELECT * FROM journey_detail_master where name  =:name and journeymasterid = :journeyMasterId LIMIT 1")
    public abstract JourneyDetailMaster getByNameMstId(String name , Long journeyMasterId);

    @Query("SELECT * FROM journey_detail_master where journeymasterid = :journeyId")
    public abstract List<JourneyDetailMaster> getListByJourneyId(Long journeyId);
}
