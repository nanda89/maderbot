package com.ebco.bot.maderbot.dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import com.ebco.bot.maderbot.model.JourneyMaster;

import java.util.List;

@Dao
public abstract class JourneyMasterDao implements BaseDao<JourneyMaster> {

    @Query("SELECT * FROM journey_master")
    public abstract List<JourneyMaster> getAll();

    @Query("SELECT * FROM journey_master where id = :id")
    public abstract JourneyMaster getById(Long id);

    @Query("SELECT * FROM journey_master where name GLOB '*' || :name || '*' and type=:type LIMIT 1")
    public abstract JourneyMaster getContainName(String name, String type);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    public abstract Long insertJourneyMaster(JourneyMaster journeyMaster);
}
