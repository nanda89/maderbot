package com.ebco.bot.maderbot;


import android.content.Context;
import android.util.Log;

import com.ebco.bot.maderbot.db.DatabaseApp;
import com.ebco.bot.maderbot.model.Journey;
import com.ebco.bot.maderbot.model.JourneyDetail;
import com.ebco.bot.maderbot.model.JourneyError;
import com.ebco.bot.maderbot.model.JourneyMaster;
import com.ebco.bot.maderbot.model.JourneyNVT;


public class DatabaseService {
    public static final String TAG = DatabaseService.class.getSimpleName();

    public static long insertIntoJourney(String token, String journey_master_id, String latitude, String longitude, String dateTime, int cellId, String type, Context context) {
        Journey journey = new Journey();
        journey.setToken(token);
        journey.setJourney_master_id(journey_master_id);
        journey.setLatitude(latitude);
        journey.setLongitude(longitude);
        journey.setDatetime(dateTime);
        journey.setIs_sent(false);
        journey.setCellId(cellId);
        journey.setType(type);
        Long id = DatabaseApp.getDatabaseApp(context).journeyDao().insertJourney(journey);
        Log.d(TAG, "insertIntoJourney: return id : " + id);
        return id;

    }


    public static long insertIntoJourneyDetail(String page, String token, String networkType, int cellId, double signalLevel, String duration, int sts, String message, String dateTime, int repeatNo, long journeyId, Long monitoringId, boolean isError, Context context) {

        JourneyDetail detail = new JourneyDetail();

        //nilai awal null akan di update berdasarkan response dari API journey
        if(monitoringId == null){
            detail.setMonitor_journey_id(0L);
        }else{
            detail.setMonitor_journey_id(monitoringId);
        }
        detail.setToken(token);

        // alias journey_detail_master_id
        detail.setJourney_detail_id(page);

        detail.setNetwork_type(networkType);
        detail.setCellid(cellId);
        detail.setSignal_level(signalLevel);
        detail.setResponse_time(duration);
        detail.setCommand(null);
        detail.setStatus(sts);
        detail.setMessage(message);
        detail.setDatetime(dateTime);
        detail.setRepeat_no(repeatNo);
        detail.setSent(false);

        detail.setJourneyid(journeyId);
        detail.setError(isError);
        Long id = DatabaseApp.getDatabaseApp(context).journeyDetailDao().insertJourneyDetail(detail);
        Log.d(TAG, "insertIntoJourneyDetail: Done");
        return id;
    }

    public static long insertIntoJourneyError(String description, long detailId,
                                              long journeyDetailId, String filename,
                                              String errorClassification, Context context) {

        JourneyError journeyError = new JourneyError();
        journeyError.setDescription(description);
        journeyError.setSent(false);
        journeyError.setDetailid(detailId);
        journeyError.setMonitor_journey_detail_id(journeyDetailId);
        journeyError.setTable(filename);
        journeyError.setErrorClassification(errorClassification);

        Log.d(TAG, "insertIntoJourneyError: Done");
        Long result = DatabaseApp.getDatabaseApp(context).journeyErrorDao().insertJourneyError(journeyError);
        Log.d(TAG, "insertIntoJourneyError: " + result);
        return result;

    }

    public static long insertIntoNVT(String token , String networkType, int cellId, double signalLevel,
                                     String duration, int sts, String message, String dateTime,
                                     int repeatNo, long journeyId, String monitoringID, int percentage,
                                     String operator,int arfn,int bandwith,int signal,int quality,
                                     int snnr,int timing,int cqi,int signalIndex,Context context) {

        JourneyNVT nvt = new JourneyNVT();
        nvt.setToken(token);

        //nilai awal null akan di update berdasarkan response dari API journey
        nvt.setMonitor_journey_id(monitoringID);

        nvt.setNetwork_type(networkType);
        nvt.setCellid(cellId);
        nvt.setSignal_level(signalLevel);
        nvt.setResponse_time(duration);
        nvt.setCommand(null);
        nvt.setStatus(sts);
        nvt.setMessage(message);
        nvt.setDatetime(dateTime);
        nvt.setRepeat_no(repeatNo);
        nvt.setSent(false);
        nvt.setPercentage(percentage);

        nvt.setOperator(operator);
        nvt.setUarfn(arfn);
        nvt.setBandwith(bandwith);
//        nvt.setCellBandwith();
        nvt.setSignalStrength(signal);
        nvt.setSignalQuality(quality);
        nvt.setSnnr(snnr);
        nvt.setTimingAdvance(timing);
        nvt.setCqi(cqi);
        nvt.setSignalIndex(signalIndex);


        // id journey local
        nvt.setJourneyid(journeyId);
        Log.d(TAG, "insertIntoNVT: Done");
        return DatabaseApp.getDatabaseApp(context).journeyNVTDao().insertNVT(nvt);
    }

    public static long insertJourneyMaster(Long id, String name, String type, Context context) {

        JourneyMaster journeyMaster = new JourneyMaster();
        journeyMaster.setId(id);
        journeyMaster.setName(name);
        journeyMaster.setType(type);
        Log.d(TAG, "insertJourneyMaster: Done");
        return DatabaseApp.getDatabaseApp(context).journeyMasterDao().insertJourneyMaster(journeyMaster);

    }
}
