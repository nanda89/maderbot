package com.ebco.bot.maderbot.dto;

public class JourneyNVTDTO extends DTO {

    /**
     * isError = true       it mean page error
     * isError = false      it mean page success
     * <p>
     * -------------- status--------------
     * 0    => success
     * 1    => error
     */

    private Long journeyId; // local ID
    private Long monitorJourneyId; // response from API journey
    private String networkType;
    private int cellId;
    private double signalLevel;
    private long responseTime;
    private String command;
    private int status;
    private String message;
    private long startTime;
    private int repeatNo;
    private int percentage;
    private boolean isSent;
    private boolean isError;
    private String token;

    public Long getJourneyId() {
        return journeyId;
    }

    public void setJourneyId(Long journeyId) {
        this.journeyId = journeyId;
    }

    public Long getMonitorJourneyId() {
        return monitorJourneyId;
    }

    public void setMonitorJourneyId(Long monitorJourneyId) {
        this.monitorJourneyId = monitorJourneyId;
    }

    public String getNetworkType() {
        return networkType;
    }

    public void setNetworkType(String networkType) {
        this.networkType = networkType;
    }

    public int getCellId() {
        return cellId;
    }

    public void setCellId(int cellId) {
        this.cellId = cellId;
    }

    public double getSignalLevel() {
        return signalLevel;
    }

    public void setSignalLevel(double signalLevel) {
        this.signalLevel = signalLevel;
    }

    public long getResponseTime() {
        return responseTime;
    }

    public void setResponseTime(long responseTime) {
        this.responseTime = responseTime;
    }

    public String getCommand() {
        return command;
    }

    public void setCommand(String command) {
        this.command = command;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public long getStartTime() {
        return startTime;
    }

    public void setStartTime(long startTime) {
        this.startTime = startTime;
    }

    public int getRepeatNo() {
        return repeatNo;
    }

    public void setRepeatNo(int repeatNo) {
        this.repeatNo = repeatNo;
    }

    public int getPercentage() {
        return percentage;
    }

    public void setPercentage(int percentage) {
        this.percentage = percentage;
    }

    public boolean isSent() {
        return isSent;
    }

    public void setSent(boolean sent) {
        isSent = sent;
    }

    public boolean isError() {
        return isError;
    }

    public void setError(boolean error) {
        isError = error;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

}
