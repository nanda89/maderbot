package com.ebco.bot.maderbot.service;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Environment;
import android.util.Log;

import androidx.annotation.Nullable;

import com.ebco.bot.maderbot.activity.ConfigActivity;
import com.ebco.bot.maderbot.db.DatabaseApp;
import com.ebco.bot.maderbot.model.Journey;
import com.ebco.bot.maderbot.model.JourneyDetail;
import com.ebco.bot.maderbot.model.JourneyError;
import com.ebco.bot.maderbot.model.JourneyNVT;
import com.ebco.bot.maderbot.pojo.ResponseGeneral;
import com.ebco.bot.maderbot.pojo.ResponseJourney;
import com.ebco.bot.maderbot.pojo.ResponseJourneyDetail;
import com.google.gson.Gson;

import java.io.File;
import java.io.IOException;
import java.util.List;

import okhttp3.FormBody;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;

public class SendDataService extends IntentService {
    public static final String TAG = "TEST-BOT";
    private String token, baseUrl;

    /**
     * Creates an IntentService.  Invoked by your subclass's constructor.
     *
     */
    public SendDataService() {
        super("SendDataService");
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        SharedPreferences sharedPreferences = getSharedPreferences(ConfigActivity.MyPREFERENCES,
                MODE_PRIVATE);
        token = sharedPreferences.getString("token", "");
        baseUrl = sharedPreferences.getString("url", "");
        sendData();
    }


    void sendData() {
        deleteData(getApplicationContext());
        List<Journey> journeys =
                DatabaseApp.getDatabaseApp(getApplicationContext()).journeyDao().getJourneyNoSent(false);

        for (Journey journey : journeys) {
            submitJourney(journey);
        }

        List<JourneyNVT> journeyNVTs =
                DatabaseApp.getDatabaseApp(getApplicationContext()).journeyNVTDao().getNVTNoSent(false);

        for (JourneyNVT journeyNVT : journeyNVTs) {
            submitNVT(journeyNVT);
        }

        List<JourneyDetail> journeyDetails =
                DatabaseApp.getDatabaseApp(getApplicationContext()).journeyDetailDao().getJourneyNoSentAll(false);


        for (JourneyDetail journeyDetail : journeyDetails) {
            submitJourneyDetail(journeyDetail);
        }

        List<JourneyError> journeyErrors =
                DatabaseApp.getDatabaseApp(getApplicationContext()).journeyErrorDao().getJourneyErrorNoSent(false);

        for (JourneyError journeyError : journeyErrors) {
            submitJourneyError(journeyError);
        }


    }


    void submitJourney(Journey journey) {
        boolean result = false;
        try {
            Log.d(TAG, "API :" + baseUrl + "/api/v1/Journey\n" +
                    "############## parameter ##############\n" +
                    "token : " + token + "\n" +
                    "cell_id : " + journey.getCellId() + " \n" +
                    "lat : " + journey.getLatitude() + "\n" +
                    "long : " + journey.getLongitude() + "\n" +
                    "datetime : " + journey.getDatetime() + "\n" +
                    "journey_master_id : " + journey.getJourney_master_id()
            );

            OkHttpClient client = new OkHttpClient();
            RequestBody formBody = new FormBody.Builder()
                    .add("journey_master_id", journey.getJourney_master_id())
                    .add("lat", String.valueOf(journey.getLatitude()))
                    .add("lng", String.valueOf(journey.getLongitude()))
                    .add("datetime", journey.getDatetime())
                    .add("cell_id", String.valueOf(journey.getCellId()))
                    .build();

            Request request = new Request.Builder()
                    .addHeader("token", token)
                    .addHeader("Content-Type", "application/x-www-form-urlencoded")
                    .url(baseUrl + "/api/v1/Journey")
                    .post(formBody)
                    .build();

            okhttp3.Response response = client.newCall(request).execute();
            Gson gson = new Gson();
            assert response.body() != null;
            String jsonData = response.body().string();
            Log.d(TAG, "submitJourney: response message journey : " + jsonData);
            ResponseJourney res = gson.fromJson(jsonData, ResponseJourney.class);
            if (res.getResponseCode() == 200) {
                Long resJourneyID = res.getData().getJourneyId();
                journey.setIs_sent(true);
                DatabaseApp.getDatabaseApp(getApplicationContext()).journeyDao().update(journey);
                List<JourneyDetail> details =
                        DatabaseApp.getDatabaseApp(getApplicationContext()).journeyDetailDao().getJourneyDetailByJournayId(journey.getId());
                Log.d(TAG, "submitJourney: total journey detail by journeyid "+journey.getId()+" : "+details.size());
                if (details.size() > 0) {

                    for (JourneyDetail journeyDetail : details) {
                        Log.d(TAG, "submitJourney: update monitor_journey_id = "+resJourneyID+" where journeyid = "+journey.getId());
                        journeyDetail.setMonitor_journey_id(resJourneyID);
                        DatabaseApp.getDatabaseApp(getApplicationContext()).journeyDetailDao().update(journeyDetail);
                    }
                }

                List<JourneyNVT> nvtList =
                        DatabaseApp.getDatabaseApp(getApplicationContext()).journeyNVTDao().getNVTByJournayId(journey.getId());
                if (nvtList.size() > 0) {
                    for (JourneyNVT nvt : nvtList) {
                        nvt.setMonitor_journey_id(String.valueOf(resJourneyID));
                        DatabaseApp.getDatabaseApp(getApplicationContext()).journeyNVTDao().update(nvt);
                    }
                }
            }


        } catch (Exception e) {
            e.printStackTrace();
            Log.e(TAG, "submitJourney: " + e.getMessage());
        }

    }

    void submitNVT(JourneyNVT journeyNVT) {
        try {

            int signalLev = 0;
            if (journeyNVT.getSignal_level() != 0) {
                signalLev = (int) journeyNVT.getSignal_level();
            }

            String uarfn =
                    journeyNVT.getUarfn() == 0 ? null : String.valueOf(journeyNVT.getUarfn());
            String bandwith =
                    journeyNVT.getBandwith() == 0 ? null : String.valueOf(journeyNVT.getBandwith());
            String signalStrength =
                    journeyNVT.getSignalStrength() == 0 ? null : String.valueOf(journeyNVT.getSignalStrength());
            String signalQuality =
                    journeyNVT.getSignalQuality() == 0 ? null : String.valueOf(journeyNVT.getSignalQuality());
            String snnr = journeyNVT.getSnnr() == 0 ? null : String.valueOf(journeyNVT.getSnnr());
            String timingAdvance =
                    journeyNVT.getTimingAdvance() == 0 ? null : String.valueOf(journeyNVT.getTimingAdvance());
            String cqi = journeyNVT.getCqi() == 0 ? null : String.valueOf(journeyNVT.getCqi());
            String signalIndex =
                    journeyNVT.getSignalIndex() == 0 ? null : String.valueOf(journeyNVT.getSignalIndex());

            Log.d(TAG, "API : " + baseUrl + "/api/v1/journeyNVT\n" +
                    "############ Parameter ############\n" +
                    "token : " + token + "\n" +
                    "journey_id : " + journeyNVT.getMonitor_journey_id() + "\n" +
                    "network_type : " + journeyNVT.getNetwork_type() + "\n" +
                    "cell_id : " + journeyNVT.getCellid() + "\n" +
                    "signal_level : " + signalLev + "\n" +
                    "response_time : " + journeyNVT.getResponse_time() + "\n" +
                    "command : \n" +
                    "status : " + journeyNVT.getStatus() + "\n" +
                    "message : " + journeyNVT.getMessage() + "\n" +
                    "datetime : " + journeyNVT.getDatetime() + "\n" +
                    "repeat_no : " + journeyNVT.getRepeat_no() + "\n" +
                    "percentage : " + journeyNVT.getPercentage() + "\n" +

                    "operator : " + journeyNVT.getOperator() + "\n" +
                    "uarfn : " + uarfn + "\n" +
                    "bandwith : " + bandwith + "\n" +
                    "signalStrength : " + signalStrength + "\n" +
                    "signalQuality : " + signalQuality + "\n" +
                    "snnr : " + snnr + "\n" +
                    "timingAdvance : " + timingAdvance + "\n" +
                    "cqi : " + cqi + "\n" +
                    "signalIndex : " + signalIndex + "\n"

            );


            OkHttpClient client = new OkHttpClient();

            FormBody.Builder formParameter = new FormBody.Builder()
                    .add("journey_id", journeyNVT.getMonitor_journey_id())
                    .add("network_type", journeyNVT.getNetwork_type())
                    .add("cell_id", String.valueOf(journeyNVT.getCellid()))
                    .add("signal_level", String.valueOf(signalLev))
                    .add("response_time", String.valueOf(journeyNVT.getResponse_time()))
                    .add("command", "")
                    .add("status", String.valueOf(journeyNVT.getStatus()))
                    .add("message", journeyNVT.getMessage())
                    .add("datetime", journeyNVT.getDatetime())
                    .add("repeat_no", String.valueOf(journeyNVT.getRepeat_no()))
                    .add("percentage", String.valueOf(journeyNVT.getPercentage()));

            if (journeyNVT.getOperator() != null) {
                formParameter.add("operator", journeyNVT.getOperator());
            }
            if (uarfn != null) {
                formParameter.add("uarfn", uarfn);
            }
            if (bandwith != null) {
                formParameter.add("bandwith", bandwith);
            }
            if (signalStrength != null) {
                formParameter.add("signalStrength", signalStrength);
            }
            if (signalQuality != null) {
                formParameter.add("signalQuality", signalQuality);
            }
            if (snnr != null) {
                formParameter.add("snnr", snnr);
            }
            if (timingAdvance != null) {
                formParameter.add("timingAdvance", timingAdvance);
            }
            if (cqi != null) {
                formParameter.add("cqi", cqi);
            }
            if (signalIndex != null) {
                formParameter.add("signalIndex", signalIndex);
            }

            RequestBody formBody = formParameter.build();
            Request request = new Request.Builder()
                    .addHeader("token", token)
                    .addHeader("Content-Type", "application/x-www-form-urlencoded")
                    .url(baseUrl + "/api/v1/journeyNVT")
                    .post(formBody)
                    .build();

            okhttp3.Response response = client.newCall(request).execute();

            Gson gson = new Gson();
            assert response.body() != null;
            String jsonData = response.body().string();
            Log.d(TAG, "doInBackground: response NVT ---> " + jsonData);
            ResponseGeneral responseMessage = gson.fromJson(jsonData, ResponseGeneral.class);

            if (responseMessage.getResponseCode() == 200) {
                journeyNVT.setSent(true);
                DatabaseApp.getDatabaseApp(getApplicationContext()).journeyNVTDao().update(journeyNVT);
            }

        } catch (Exception e) {
            e.printStackTrace();
            Log.e(TAG, "submitNVT: " + e.getMessage());
        }
    }

    void submitJourneyDetail(JourneyDetail journeyDetail) {

        try {

            int signalLev = 0;
            if (journeyDetail.getSignal_level() != 0) {
                signalLev = (int) journeyDetail.getSignal_level();
            }

            Log.d(TAG, "API : " + baseUrl + "/api/v1/journeyDetail\n" +
                    "############ Parameter ############\n" +
                    "token : " + token + "\n" +
                    "journey_id : " + journeyDetail.getMonitor_journey_id() + "\n" +
                    "journey_detail_master_id : " + journeyDetail.getJourney_detail_id() + "\n" +
                    "cell_id : " + journeyDetail.getCellid() + "\n" +
                    "network_type : " + journeyDetail.getNetwork_type() + "\n" +
                    "signal_level : " + signalLev + "\n" +
                    "response_time : " + journeyDetail.getResponse_time() + "\n" +
                    "command : \n" +
                    "status : " + journeyDetail.getStatus() + "\n" +
                    "message : " + journeyDetail.getMessage() + "\n" +
                    "datetime : " + journeyDetail.getDatetime() + "\n" +
                    "repeat_no : " + journeyDetail.getRepeat_no() + "\n");

            OkHttpClient client = new OkHttpClient();
            RequestBody formBody = new FormBody.Builder()
                    .add("journey_id", String.valueOf(journeyDetail.getMonitor_journey_id()))
                    .add("journey_detail_master_id", journeyDetail.getJourney_detail_id())
                    .add("network_type", journeyDetail.getNetwork_type())
                    .add("cell_id", String.valueOf(journeyDetail.getCellid()))
                    .add("signal_level", String.valueOf(signalLev))
                    .add("response_time", journeyDetail.getResponse_time())
                    .add("command", "")
                    .add("status", String.valueOf(journeyDetail.getStatus()))
                    .add("message", journeyDetail.getMessage())
                    .add("datetime", journeyDetail.getDatetime())
                    .add("repeat_no", String.valueOf(journeyDetail.getRepeat_no()))
                    .build();


            Request request = new Request.Builder()
                    .addHeader("token", token)
                    .addHeader("Content-Type", "application/x-www-form-urlencoded")
                    .url(baseUrl + "/api/v1/journeyDetail")
                    .post(formBody)
                    .build();

            okhttp3.Response response = client.newCall(request).execute();

            Gson gson = new Gson();
            assert response.body() != null;
            String jsonData = response.body().string();
            Log.d(TAG, "doInBackground: response message journey Detail: " + jsonData);
            ResponseJourneyDetail responseMessage =
                    gson.fromJson(jsonData, ResponseJourneyDetail.class);

            if (responseMessage.getResponseCode() == 200) {
                Long journeyDetailId = responseMessage.getData().getJourneyDetailId();
                Log.d(TAG, "doInBackground: journeyDetailID " + journeyDetailId);
                journeyDetail.setSent(true);
                DatabaseApp.getDatabaseApp(getApplicationContext()).journeyDetailDao().update(journeyDetail);

                if (journeyDetail.isError()) {
                    JourneyError journeyError =
                            DatabaseApp.getDatabaseApp(getApplicationContext()).journeyErrorDao().getJourneyErrByDetailId(journeyDetail.getId());
                    journeyError.setMonitor_journey_detail_id(journeyDetailId);
                    DatabaseApp.getDatabaseApp(getApplicationContext()).journeyErrorDao().update(journeyError);
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
            Log.e(TAG, "submitJourneyDetail: " + e.getMessage());
        }
    }

    void submitJourneyError(JourneyError journeyError) {
        File fileError =
                new File(Environment.getExternalStorageDirectory() + "/JeniusAPM/" + journeyError.getTable());
        if (fileError.exists()) {
            try {
                Log.d(TAG, "API :" + baseUrl + "/api/v1/journeyError\n" +
                        "############## parameter ##############\n" +
                        "journey_detail_id : " + journeyError.getMonitor_journey_detail_id() + "\n" +
                        "description : " + journeyError.getDescription() + "\n" +
                        "image : " + journeyError.getTable() + "\n" +
                        "error_clasification: " + journeyError.getErrorClassification()
                );

                OkHttpClient client = new OkHttpClient();

                RequestBody formBody = new MultipartBody.Builder().setType(MultipartBody.FORM)
                        .addFormDataPart("image", fileError.getName(),
                                RequestBody.create(MediaType.parse("image/*"), fileError))
                        .addFormDataPart("journey_detail_id", journeyError.getMonitor_journey_detail_id().toString())
                        .addFormDataPart("description", journeyError.getDescription())
                        .addFormDataPart("error_clasification", journeyError.getErrorClassification())
                        .build();

                Request request = new Request.Builder()
                        .addHeader("token", token)
                        .addHeader("Content-Type", "application/x-www-form-urlencoded")
                        .url(baseUrl + "/api/v1/journeyError")
                        .post(formBody)
                        .build();

                okhttp3.Response response = client.newCall(request)
                        .execute();
                Gson gson = new Gson();
                assert response.body() != null;
                String jsonData = response.body().string();
                Log.d(TAG, "doInBackground: response message journey : " + jsonData);
                ResponseGeneral res = gson.fromJson(jsonData, ResponseGeneral.class);
                Log.d(TAG, "doInBackground: responseCode : " + res.getResponseCode());
                if (res.getResponseCode() == 200) {
                    journeyError.setSent(true);
                    DatabaseApp.getDatabaseApp(getApplicationContext()).journeyErrorDao().update(journeyError);
                }

            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            try {
                Log.d(TAG, "API :" + baseUrl + "/api/v1/journeyError\n" +
                        "############## parameter ##############\n" +
                        "journey_detail_id : " + journeyError.getMonitor_journey_detail_id() + "\n" +
                        "description : " + journeyError.getDescription() + "\n" +
                        "image : " + journeyError.getTable() + "\n" +
                        "error_clasification: " + journeyError.getErrorClassification()
                );

                OkHttpClient client = new OkHttpClient();

                RequestBody formBody = new MultipartBody.Builder().setType(MultipartBody.FORM)
                        .addFormDataPart("journey_detail_id", journeyError.getMonitor_journey_detail_id().toString())
                        .addFormDataPart("description", journeyError.getDescription())
                        .addFormDataPart("error_clasification", journeyError.getErrorClassification())
                        .build();

                Request request = new Request.Builder()
                        .addHeader("token", token)
                        .addHeader("Content-Type", "application/x-www-form-urlencoded")
                        .url(baseUrl + "/api/v1/journeyError")
                        .post(formBody)
                        .build();

                okhttp3.Response response = client.newCall(request)
                        .execute();
                Gson gson = new Gson();
                assert response.body() != null;
                String jsonData = response.body().string();
                Log.d(TAG, "doInBackground: response message journey : " + jsonData);
                ResponseGeneral res = gson.fromJson(jsonData, ResponseGeneral.class);
                Log.d(TAG, "doInBackground: responseCode : " + res.getResponseCode());
                if (res.getResponseCode() == 200) {
                    journeyError.setSent(true);
                    DatabaseApp.getDatabaseApp(getApplicationContext()).journeyErrorDao().update(journeyError);
                }

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }


    private static void deleteData(Context context) {
        /***
         *
         * check data for journey detail :
         * 1.delete data when journey_detail_id = 0
         * 2.delete data when monitoring_journey_id = 0 if journey isSent = true
         */
        DatabaseApp.getDatabaseApp(context).journeyDetailDao().deleteByJourneyDetailId();

        List<JourneyDetail> details =
                DatabaseApp.getDatabaseApp(context).journeyDetailDao().getByMonitoringId();
        for (JourneyDetail detail : details) {
            Journey journey =
                    DatabaseApp.getDatabaseApp(context).journeyDao().getJourneyId(detail.getJourneyid());
            if (journey != null) {
                if (journey.isIs_sent()) {
                    DatabaseApp.getDatabaseApp(context).journeyDetailDao().deleteById(detail.getId());
                }
            }
        }
        /**
         check data for journey_error :
         1.delete data when monitor_journey_detail_id = 0 if journeyDetail isSent = true
         */
        List<JourneyError> errors =
                DatabaseApp.getDatabaseApp(context).journeyErrorDao().getByMonitoring();
        for (JourneyError error : errors) {
            JourneyDetail detail =
                    DatabaseApp.getDatabaseApp(context).journeyDetailDao().getById(error.getDetailid());
            if (detail != null) {
                if (detail.isSent()) {
                    DatabaseApp.getDatabaseApp(context).journeyErrorDao().deleteById(error.getId());
                }
            }
        }

    }

}
