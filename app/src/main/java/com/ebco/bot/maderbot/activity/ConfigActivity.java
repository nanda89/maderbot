package com.ebco.bot.maderbot.activity;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.ebco.bot.maderbot.R;

public class ConfigActivity extends AppCompatActivity {

    public static final String TAG = ConfigActivity.class.getSimpleName();
    public static final String MyPREFERENCES = "MyPref";
    SharedPreferences sharedPreferences;

    EditText etUrl;
    EditText etToken;
    EditText etPassword;
    EditText etPin;
    EditText etIntervalPage;
    EditText etIntervalJourney;
    EditText etDownloadAppURL;
    EditText etDownloadBotURL;
    EditText etWaitTimeOut;
    EditText etKtp;
    EditText etAccount;
    EditText etCardNumber;
    EditText etExpireCard;
    Button btnSave;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_config);

        etUrl = findViewById(R.id.url);
        etToken = findViewById(R.id.token);
        etPassword = findViewById(R.id.password);
        etPin = findViewById(R.id.pin);
        etIntervalPage = findViewById(R.id.intervalPage);
        etIntervalJourney = findViewById(R.id.intervalJourneyPage);
        etDownloadAppURL = findViewById(R.id.downloadAppURL);
        etDownloadBotURL = findViewById(R.id.downloadBotURL);
        etWaitTimeOut = findViewById(R.id.waitTimeOutPage);
        etKtp = findViewById(R.id.ktp);
        etAccount = findViewById(R.id.account);
        etCardNumber = findViewById(R.id.debitCard);
        etExpireCard = findViewById(R.id.expireCard);
        btnSave = findViewById(R.id.btnSave);
        sharedPreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                saveToPref();
            }
        });
    }

    private void saveToPref() {

        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("url", etUrl.getText().toString());
        editor.putString("token", etToken.getText().toString());
        editor.putString("pin", etPin.getText().toString());
        editor.putString("loginPassword", etPassword.getText().toString());
        editor.putInt("journey_interval", Integer.valueOf(etIntervalJourney.getText().toString()));
        editor.putInt("page_interval", Integer.valueOf(etIntervalPage.getText().toString()));
        editor.putString("downloadApp", etDownloadAppURL.getText().toString());
        editor.putString("downloadBot", etDownloadBotURL.getText().toString());
        editor.putString("waitTimeOut", etWaitTimeOut.getText().toString());

        editor.putString("ktp", etKtp.getText().toString());
        editor.putString("account", etAccount.getText().toString());
        editor.putString("debit_card", etCardNumber.getText().toString());
        editor.putString("expire_card", etExpireCard.getText().toString());

        editor.commit();

        Toast.makeText(ConfigActivity.this , "Data berhasil diperbaharui.", Toast.LENGTH_LONG).show();
    }

    @Override
    protected void onResume() {
        super.onResume();

        String prefUrl = sharedPreferences.getString("url", "");
        String prefToken = sharedPreferences.getString("token", "");
        String prefPIN = sharedPreferences.getString("pin", "");
        String prefPassword = sharedPreferences.getString("loginPassword", "");
        int prefIntervalPage = sharedPreferences.getInt("page_interval", 0);
        int prefIntervalJourney = sharedPreferences.getInt("journey_interval", 0);
        String downloadApp = sharedPreferences.getString("downloadApp", "");
        String downloadBot = sharedPreferences.getString("downloadBot", "");
        String waitTimeOut = sharedPreferences.getString("waitTimeOut", "");

        String ktp = sharedPreferences.getString("ktp", "");
        String account = sharedPreferences.getString("account", "");
        String cardNumber = sharedPreferences.getString("debit_card", "");
        String expireCard = sharedPreferences.getString("expire_card", "");


        etUrl.setText(prefUrl);
        etToken.setText(prefToken);
        etPin.setText(prefPIN);
        etPassword.setText(prefPassword);
        etIntervalJourney.setText(String.valueOf(prefIntervalJourney));
        etIntervalPage.setText(String.valueOf(prefIntervalPage));
        etDownloadAppURL.setText(downloadApp);
        etDownloadBotURL.setText(downloadBot);
        etWaitTimeOut.setText(waitTimeOut);

        etKtp.setText(ktp);
        etAccount.setText(account);
        etCardNumber.setText(cardNumber);
        etExpireCard.setText(expireCard);

    }
}