package com.ebco.bot.maderbot.pojo;

public class ResponseJourney {

    private int responseCode;
    private boolean status;
    private String message;
    private Data data;


    public static class Data{
        private Long journeyId;

        public Long getJourneyId() {
            return journeyId;
        }

        public void setJourneyId(Long journeyId) {
            this.journeyId = journeyId;
        }
    }

    public int getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(int responseCode) {
        this.responseCode = responseCode;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }
}
