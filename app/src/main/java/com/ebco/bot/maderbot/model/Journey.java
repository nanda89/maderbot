package com.ebco.bot.maderbot.model;

import androidx.room.Entity;
import androidx.room.Index;

@Entity(tableName = "journey",indices = {@Index("id")})
public class Journey extends ModelEntity {

    private String token;
    /*
        response from API Journey Master
     */
    private String journey_master_id;

    private String latitude;
    private String longitude;
    private String datetime;
    private int cellId;
    private boolean is_sent;

    /*
        contain : mobile or web
     */
    private String type;

    public boolean isIs_sent() {
        return is_sent;
    }

    public void setIs_sent(boolean is_sent) {
        this.is_sent = is_sent;
    }

    public String getToken() {
        return token;
    }


    public String getLatitude() {
        return latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public String getDatetime() {
        return datetime;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getJourney_master_id() {
        return journey_master_id;
    }

    public void setJourney_master_id(String journey_master_id) {
        this.journey_master_id = journey_master_id;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public int getCellId() {
        return cellId;
    }

    public void setCellId(int cellId) {
        this.cellId = cellId;
    }

    public void setDatetime(String datetime) {
        this.datetime = datetime;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
