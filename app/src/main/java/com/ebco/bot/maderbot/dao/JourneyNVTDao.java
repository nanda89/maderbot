package com.ebco.bot.maderbot.dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import com.ebco.bot.maderbot.model.JourneyNVT;

import java.util.List;

@Dao
public abstract class JourneyNVTDao implements BaseDao<JourneyNVT> {

    @Query("SELECT * FROM journey_nvt where id = :id")
    public abstract JourneyNVT getNVTById(Long id);

    @Query("SELECT * FROM journey_nvt where monitor_journey_id <> null and isError = :isError")
    public abstract List<JourneyNVT> getNVTByMonitorNotNull(boolean isError);

    @Query("SELECT * FROM journey_nvt")
    public abstract List<JourneyNVT> getAllNVT();

    @Query("SELECT * FROM journey_nvt where journeyid = :journeyId")
    public abstract List<JourneyNVT> getNVTByJournayId(Long journeyId);

    @Query("SELECT * FROM journey_nvt where isSent = :isSent order by id")
    public abstract List<JourneyNVT> getNVTNoSent(boolean isSent);

    @Query("SELECT * FROM journey_nvt where isSent = :isSent and journeyid = :journeyId order by id desc limit 1")
    public abstract JourneyNVT getNVTNoSentByJourneyId(boolean isSent , long journeyId);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    public abstract Long insertNVT(JourneyNVT nvt);

    @Query("DELETE FROM journey_nvt")
    public abstract void deleteAll();
}
