package com.ebco.bot.maderbot.dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import com.ebco.bot.maderbot.model.Journey;

import java.util.List;


@Dao
public abstract class JourneyDao implements BaseDao<Journey> {

    @Query("SELECT * FROM journey where id = :id")
    public abstract Journey getJourneyId(Long id);

    @Query("SELECT * FROM journey  order by id desc limit 1")
    public abstract Journey getLastJourney();

    @Query("SELECT * FROM journey")
    public abstract List<Journey> getAllJourney();

    @Query("SELECT * FROM journey where is_sent = :isSent order by id limit 1000")
    public abstract List<Journey> getJourneyNoSent(boolean isSent);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    public abstract Long insertJourney(Journey journey);

    @Query("DELETE FROM journey")
    public abstract void deleteAll();
}
