package com.ebco.bot.maderbot.dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import com.ebco.bot.maderbot.model.JourneyDetail;

import java.util.List;

@Dao
public abstract class JourneyDetailDao implements BaseDao<JourneyDetail> {

    @Query("SELECT * FROM journey_detail where id = :id")
    public abstract JourneyDetail getById(Long id);

    @Query("SELECT * FROM journey_detail where id = :id")
    public abstract JourneyDetail getJourneyDetailId(Long id);

    @Query("SELECT * FROM journey_detail where isError = :isError")
    public abstract List<JourneyDetail> getJourneyDetailByMonitorNotNull(boolean isError);

    @Query("SELECT * FROM journey_detail where journeyid =:journeyId")
    public abstract List<JourneyDetail> getByJourneyId(long journeyId);

    @Query("SELECT * FROM journey_detail")
    public abstract List<JourneyDetail> getAllJourneyDetail();

    @Query("SELECT * FROM journey_detail where monitor_journey_id = 0 order by id asc")
    public abstract List<JourneyDetail> getByMonitoringId();

    @Query("SELECT * FROM journey_detail where journeyid = :journeyId")
    public abstract List<JourneyDetail> getJourneyDetailByJournayId(Long journeyId);

    @Query("SELECT * FROM journey_detail where isSent = :isSent AND monitor_journey_id != NULL order by id asc")
    public abstract List<JourneyDetail> getJourneyNoSent(boolean isSent);


    @Query("SELECT * FROM journey_detail where isSent = :isSent and monitor_journey_id > 0 order by id asc")
    public abstract List<JourneyDetail> getJourneyNoSentAll(boolean isSent);

    @Query("SELECT * FROM journey_detail where isSent = :isSent and journeyid = :journeyId order by id asc limit 100")
    public abstract List<JourneyDetail> getJourneyNoSentByJourneyId(boolean isSent , long journeyId);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    public abstract Long insertJourneyDetail(JourneyDetail journeyDetail);

    @Query("DELETE FROM journey_detail")
    public abstract void deleteAll();

    @Query("DELETE FROM journey_detail where id =:id")
    public abstract void deleteById(long id);

    @Query("DELETE FROM journey_detail where journey_detail_id = 0")
    public abstract void deleteByJourneyDetailId();
}
