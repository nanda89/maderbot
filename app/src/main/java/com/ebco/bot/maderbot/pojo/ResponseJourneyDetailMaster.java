package com.ebco.bot.maderbot.pojo;


import com.ebco.bot.maderbot.model.JourneyMaster;
import com.ebco.bot.maderbot.model.ModelEntity;

import java.util.List;

public class ResponseJourneyDetailMaster {

    private int responseCode;
    private boolean status;
    private Data data;


    public static class Data extends ModelEntity {

        private List<JourneyDetailMaster> journeyDetailMasters;

        public List<JourneyDetailMaster> getJourneyDetailMasters() {
            return journeyDetailMasters;
        }

        public void setJourneyDetailMasters(List<JourneyDetailMaster> journeyDetailMasters) {
            this.journeyDetailMasters = journeyDetailMasters;
        }
    }

    public static class JourneyDetailMaster extends ModelEntity{
        private String name;
        private JourneyMaster journeyMaster;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public JourneyMaster getJourneyMaster() {
            return journeyMaster;
        }

        public void setJourneyMaster(JourneyMaster journeyMaster) {
            this.journeyMaster = journeyMaster;
        }
    }

    public int getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(int responseCode) {
        this.responseCode = responseCode;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }
}
