package com.ebco.bot.maderbot.dao;


import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import com.ebco.bot.maderbot.model.JourneyError;

import java.util.List;

@Dao
public abstract class JourneyErrorDao implements BaseDao<JourneyError> {

    @Query("SELECT * FROM journey_error LIMIT 100")
    public abstract List<JourneyError> getAll();

    @Query("SELECT * FROM journey_error WHERE isSent = :isSent AND detailid IN(:detailId) LIMIT 100")
    public abstract List<JourneyError> gettNotSent(boolean isSent , List<Long> detailId);

    @Query("SELECT * FROM journey_error where id = :id")
    public abstract JourneyError getJourneyErrById(Long id);

    @Query("SELECT * FROM journey_error where detailid = :id")
    public abstract JourneyError getJourneyErrByDetailId(Long id);

    @Query("SELECT * FROM journey_error where isSent = :isSent order by id")
    public abstract List<JourneyError> getJourneyErrorNoSent(boolean isSent);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    public abstract Long insertJourneyError(JourneyError journeyError);

    @Query("DELETE FROM journey_error")
    public abstract void deleteAll();

    @Query("DELETE FROM journey_error where id =:id")
    public abstract void deleteById(long id);

    @Query("SELECT * FROM journey_error where monitor_journey_detail_id = 0")
    public abstract List<JourneyError> getByMonitoring();
}
