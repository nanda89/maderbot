package com.ebco.bot.maderbot.model;

import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.Index;

@Entity(tableName = "journey_detail", foreignKeys = @ForeignKey(entity = Journey.class, parentColumns =
        "id", childColumns = "journeyid", onDelete = ForeignKey.CASCADE, onUpdate = ForeignKey
        .CASCADE), indices = @Index({"id", "journeyid"}))
public class JourneyDetail extends ModelEntity {

    /**
     * isError = true       it mean page error
     * isError = false      it mean page success
     */

    private String token;
    /*
        response from API journey
     */
    private Long monitor_journey_id;

    private String network_type;
    private int cellid;
    private double signal_level;
    private String response_time;
    private String command;
    private int status;
    private String message;
    private String datetime;
    private int repeat_no;
    /*
        response from API journey detail master
     */
    private String journey_detail_id;

    //local id
    private Long journeyid;
    private boolean isSent;
    private boolean isError;

    public boolean isSent() {
        return isSent;
    }

    public void setSent(boolean sent) {
        isSent = sent;
    }

    public String getResponse_time() {
        return response_time;
    }

    public void setResponse_time(String response_time) {
        this.response_time = response_time;
    }

    public String getNetwork_type() {
        return network_type;
    }

    public void setNetwork_type(String network_type) {
        this.network_type = network_type;
    }

    public boolean isError() {
        return isError;
    }

    public void setError(boolean error) {
        isError = error;
    }

    public Long getJourneyid() {
        return journeyid;
    }

    public void setJourneyid(Long journeyid) {
        this.journeyid = journeyid;
    }

    public String getToken() {
        return token;
    }



    public String getJourney_detail_id() {
        return journey_detail_id;
    }

    public int getCellid() {
        return cellid;
    }

    public double getSignal_level() {
        return signal_level;
    }

    public String getCommand() {
        return command;
    }

    public int getStatus() {
        return status;
    }

    public String getMessage() {
        return message;
    }

    public String getDatetime() {
        return datetime;
    }

    public int getRepeat_no() {
        return repeat_no;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public Long getMonitor_journey_id() {
        return monitor_journey_id;
    }

    public void setMonitor_journey_id(Long monitor_journey_id) {
        this.monitor_journey_id = monitor_journey_id;
    }

    public void setJourney_detail_id(String journey_detail_id) {
        this.journey_detail_id = journey_detail_id;
    }

    public void setCellid(int cellid) {
        this.cellid = cellid;
    }

    public void setSignal_level(double signal_level) {
        this.signal_level = signal_level;
    }

    public void setCommand(String command) {
        this.command = command;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public void setDatetime(String datetime) {
        this.datetime = datetime;
    }

    public void setRepeat_no(int repeat_no) {
        this.repeat_no = repeat_no;
    }
}
