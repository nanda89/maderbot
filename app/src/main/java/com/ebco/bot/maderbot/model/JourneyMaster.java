package com.ebco.bot.maderbot.model;


import androidx.room.Entity;
import androidx.room.Index;

@Entity(tableName = "journey_master",indices = {@Index("id")})
public class JourneyMaster extends ModelEntity{

    private String name;
    private String type;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
