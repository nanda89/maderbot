package com.ebco.bot.maderbot.pojo;

import java.util.List;

public class ResponseMessage {

    private int status;
    private int affected_rows;
    private int num_rows;
    private int num_cols;
    private List<Rows> rows;
    private String error;
    private String message;


    public static class Rows {
        private String id;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }
    }

    public int getStatus() {
        return status;
    }

    public int getAffected_rows() {
        return affected_rows;
    }

    public int getNum_rows() {
        return num_rows;
    }

    public int getNum_cols() {
        return num_cols;
    }

    public List<Rows> getRows() {
        return rows;
    }

    public String getError() {
        return error;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public void setAffected_rows(int affected_rows) {
        this.affected_rows = affected_rows;
    }

    public void setNum_rows(int num_rows) {
        this.num_rows = num_rows;
    }

    public void setNum_cols(int num_cols) {
        this.num_cols = num_cols;
    }

    public void setRows(List<Rows> rows) {
        this.rows = rows;
    }

    public void setError(String error) {
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
