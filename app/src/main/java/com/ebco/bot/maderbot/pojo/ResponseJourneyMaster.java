package com.ebco.bot.maderbot.pojo;


import com.ebco.bot.maderbot.model.JourneyMaster;

import java.util.List;

public class ResponseJourneyMaster {

    private int responseCode;
    private boolean status;
    private Data data;


    public static class Data{
        List<JourneyMaster> journeyMasters;

        public List<JourneyMaster> getJourneyMasters() {
            return journeyMasters;
        }

        public void setJourneyMasters(List<JourneyMaster> journeyMasters) {
            this.journeyMasters = journeyMasters;
        }
    }

    public int getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(int responseCode) {
        this.responseCode = responseCode;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }
}
