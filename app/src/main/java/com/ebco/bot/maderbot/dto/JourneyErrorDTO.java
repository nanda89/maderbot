package com.ebco.bot.maderbot.dto;

public class JourneyErrorDTO extends DTO {

    private Long detailId;
    private Long monitorJourneyDetailId = 0L;
    private String description;
    private String imageFilename;
    private String errorClassification = "Implicit";

    public Long getDetailId() {
        return detailId;
    }

    public void setDetailId(Long detailId) {
        this.detailId = detailId;
    }

    public Long getMonitorJourneyDetailId() {
        return monitorJourneyDetailId;
    }

    public void setMonitorJourneyDetailId(Long monitorJourneyDetailId) {
        this.monitorJourneyDetailId = monitorJourneyDetailId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImageFilename() {
        return imageFilename;
    }

    public void setImageFilename(String imageFilename) {
        this.imageFilename = imageFilename;
    }

    public String getErrorClassification() {
        return errorClassification;
    }

    public void setErrorClassification(String errorClassification) {
        this.errorClassification = errorClassification;
    }

}
