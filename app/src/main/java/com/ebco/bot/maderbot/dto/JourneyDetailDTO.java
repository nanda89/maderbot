package com.ebco.bot.maderbot.dto;

public class JourneyDetailDTO extends DTO {

    private Long journeyId;
    private Long journeyDetailMasterId;
    private Long monitorJourneyId = 0L;
    private long startTime;
    private String location;
    private String operator;
    private String journeyName;
    private String detailName;
    private int cellId;
    private Double latitude;
    private Double longitude;
    private String networkType;
    private double signalLevel;
    private long responseTime;
    private int status;
    private String message;
    private boolean error;
    private double nvtSignal;
    private long nvtResponseTime;
    private String token;
    private int repeatNo;

    public Long getJourneyId() {
        return journeyId;
    }

    public void setJourneyId(Long journeyId) {
        this.journeyId = journeyId;
    }

    public Long getJourneyDetailMasterId() {
        return journeyDetailMasterId;
    }

    public void setJourneyDetailMasterId(Long journeyDetailMasterId) {
        this.journeyDetailMasterId = journeyDetailMasterId;
    }

    public Long getMonitorJourneyId() {
        return monitorJourneyId;
    }

    public void setMonitorJourneyId(Long monitorJourneyId) {
        this.monitorJourneyId = monitorJourneyId;
    }

    public long getStartTime() {
        return startTime;
    }

    public void setStartTime(long startTime) {
        this.startTime = startTime;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getOperator() {
        return operator;
    }

    public void setOperator(String operator) {
        this.operator = operator;
    }

    public String getJourneyName() {
        return journeyName;
    }

    public void setJourneyName(String journeyName) {
        this.journeyName = journeyName;
    }

    public String getDetailName() {
        return detailName;
    }

    public void setDetailName(String detailName) {
        this.detailName = detailName;
    }

    public int getCellId() {
        return cellId;
    }

    public void setCellId(int cellId) {
        this.cellId = cellId;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public String getNetworkType() {
        return networkType;
    }

    public void setNetworkType(String networkType) {
        this.networkType = networkType;
    }

    public double getSignalLevel() {
        return signalLevel;
    }

    public void setSignalLevel(double signalLevel) {
        this.signalLevel = signalLevel;
    }

    public long getResponseTime() {
        return responseTime;
    }

    public void setResponseTime(long responseTime) {
        this.responseTime = responseTime;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    public double getNvtSignal() {
        return nvtSignal;
    }

    public void setNvtSignal(double nvtSignal) {
        this.nvtSignal = nvtSignal;
    }

    public long getNvtResponseTime() {
        return nvtResponseTime;
    }

    public void setNvtResponseTime(long nvtResponseTime) {
        this.nvtResponseTime = nvtResponseTime;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public int getRepeatNo() {
        return repeatNo;
    }

    public void setRepeatNo(int repeatNo) {
        this.repeatNo = repeatNo;
    }
}
